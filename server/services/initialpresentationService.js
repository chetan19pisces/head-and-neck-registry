const initialpresentationModel = require("../models/initialpresentationModel");

class InitialPresentationService {

  //Find Initial Presentation by code
  async findInitalPresentation(filter) {    
    const initial = await initialpresentationModel.find({ code: filter });
    return initial;
  }

  //Create InitialPresentation
  async submitInitialPresentation(data) {
    const user = await initialpresentationModel.create(data);
    return user;
  }

  //Update InitialPresentation
  async updateInitialPresentation(filter, data) {        
    const user = await initialpresentationModel.findOneAndUpdate({ code: filter }, data);
    return user;
  }
}

module.exports = new InitialPresentationService();

const AWS = require("aws-sdk");
const accessKeyId = process.env.AWS_ACCESS_KEY_ID;
const secretAccessKey = process.env.AWS_SECRET_ACCESS_KEY;

const SES_CONFIG = {
  accessKeyId: accessKeyId,
  secretAccessKey: secretAccessKey,
  region: "ap-south-1",
};

const AWS_SES = new AWS.SES(SES_CONFIG);

class AdminEmailService {
  //Send Forgot Password Link

  async sendForgotPasswordLink(recipientEmail, name, accessToken) {
    let params = {
      Source: "Head & Neck <rahul.k@riverroute.in>",
      Destination: {
        ToAddresses: [`${recipientEmail}`],
      },
      ReplyToAddresses: [],
      Message: {
        Body: {
          Html: {
            Charset: "UTF-8",
            Data: `<html xmlns="http://www.w3.org/1999/xhtml"><head><meta http-equiv="content-type" content="text/html; charset=utf-8"><meta name="viewport" content="width=device-width,initial-scale=1;"><meta name="format-detection" content="telephone=no"><style>body{margin:0;padding:0;min-width:100%;width:100%!important;height:100%!important}a,body,div,p,table,td{-webkit-font-smoothing:antialiased;text-size-adjust:100%;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;line-height:100%}table,td{mso-table-lspace:0;mso-table-rspace:0;border-collapse:collapse!important;border-spacing:0}img{border:0;line-height:100%;outline:0;text-decoration:none;-ms-interpolation-mode:bicubic}#outlook a{padding:0}.ReadMsgBody{width:100%}.ExternalClass{width:100%}.ExternalClass,.ExternalClass div,.ExternalClass font,.ExternalClass p,.ExternalClass span,.ExternalClass td{line-height:100%}@media all and (min-width:560px){.container{border-radius:8px;-webkit-border-radius:8px;-moz-border-radius:8px;-khtml-border-radius:8px}}a,a:hover{color:#127db3}.footer a,.footer a:hover{color:#999}</style></head><body topmargin="0" rightmargin="0" bottommargin="0" leftmargin="0" marginwidth="0" marginheight="0" width="100%" style="border-collapse:collapse;border-spacing:0;margin:0;padding:0;width:100%;height:100%;-webkit-font-smoothing:antialiased;text-size-adjust:100%;-ms-text-size-adjust:100%;-webkit-text-size-adjust:100%;line-height:100%;background-color:#f0f0f0;color:#000" bgcolor="#F0F0F0" text="#000000"><table class="background" style="border-collapse:collapse;border-spacing:0;margin:0;padding:0;width:100%" border="0" width="100%" cellspacing="0" cellpadding="0" align="center"><tbody><tr><td style="border-collapse:collapse;border-spacing:0;margin:0;padding:0" align="center" valign="top" bgcolor="#F0F0F0"><table class="wrapper" style="border-collapse:collapse;border-spacing:0;padding:0;width:inherit;max-width:560px" border="0" width="560" cellspacing="0" cellpadding="0" align="center"><tbody><tr><td class="hero" style="border-collapse:collapse;border-spacing:0;margin:0;padding:0;padding-top:20px" align="center" valign="top"><img style="width:100%;max-width:560px;color:#000;font-size:13px;margin:0;padding:0;outline:0;text-decoration:none;-ms-interpolation-mode:bicubic;border:none;display:block" title="Hero Image" src="https://onconavigator.s3.ap-south-1.amazonaws.com/assets/emailers/emailer-top.jpg" alt="Head & Neck Onconavigator Web Application" width="560" border="0" hspace="0" vspace="0"></td></tr></tbody></table><table class="container" style="border-collapse:collapse;border-spacing:0;padding:0;width:inherit;max-width:560px" border="0" width="560" cellspacing="0" cellpadding="0" align="center" bgcolor="#FFFFFF"><tbody><tr><td class="hero" style="border-collapse:collapse;border-spacing:0;margin:0;padding:0;padding-top:20px" align="center" valign="top"><img style="width:100%;max-width:560px;color:#000;font-size:13px;margin:0;padding:0;outline:0;text-decoration:none;-ms-interpolation-mode:bicubic;border:none;display:block" title="Onconavigator" src="https://onconavigator.s3.ap-south-1.amazonaws.com/assets/emailers/emailer-banner.jpg" alt="Onconavigator" width="560" border="0" hspace="0" vspace="0"></td></tr><tr><td class="paragraph" style="border-collapse:collapse;border-spacing:0;margin:0;padding:0;padding-left:6.25%;padding-right:6.25%;width:87.5%;font-size:17px;font-weight:400;line-height:160%;padding-top:25px;color:#000;font-family:sans-serif" align="center" valign="top">Dear, ${name}<br>Forgot your password? That\'s okay. You can create your new password here..<br><table><tbody><tr><td style="padding:12px 24px;margin:0;text-decoration:underline;border-collapse:collapse;border-spacing:0;border-radius:4px;-webkit-border-radius:4px;-moz-border-radius:4px;-khtml-border-radius:4px" align="center" valign="middle" bgcolor="#E9703E"><a style="text-decoration:none;color:#fff;font-family:sans-serif;font-size:17px;font-weight:400;line-height:120%;text-decoration:none" href="${process.env.APP_BASE_URL}/admin/create-password/${accessToken.accessToken}/${recipientEmail}" target="_blank">Create Password</a></td></tr></tbody></table><table><tr><td><br></td></tr></table><table style="width:100%;margin:0 auto"><tbody><tr><td style="width:100%; font-size:12px; line-height:150%">Button not working? Try pasting this URL into your browser:<br><a href="${process.env.APP_BASE_URL}/admin/create-password/${accessToken.accessToken}/${recipientEmail}">${process.env.APP_BASE_URL}/admin/create-password/${accessToken.accessToken}/${recipientEmail}</a></td></tr></tbody></table></td></tr><tr><td class="line" style="border-collapse:collapse;border-spacing:0;margin:0;padding:0;padding-left:6.25%;padding-right:6.25%;width:87.5%;padding-top:25px" align="center" valign="top"><hr style="margin:0;padding:0" align="center" noshade="noshade" size="1" width="100%"></td></tr><tr><td class="paragraph" style="border-collapse:collapse;border-spacing:0;margin:0;padding:20px 6.25% 25px 6.25%;width:87.5%;font-size:17px;font-weight:400;line-height:160%;color:#000;font-family:sans-serif" align="center" valign="top">Have a question? <a style="color:#127db3;font-family:sans-serif;font-size:17px;font-weight:400;line-height:160%" href="mailto:support@onconavigator.in" target="_blank">support@onconavigator.in</a></td></tr></tbody></table></td></tr></tbody></table><table class="wrapper" style="border-collapse:collapse;border-spacing:0;padding:0;width:inherit;max-width:560px" border="0" width="560" cellspacing="0" cellpadding="0" align="center"><tbody><tr><td class="footer" style="border-collapse:collapse;border-spacing:0;margin:0;padding:20px 6.25% 20px 6.25%;width:87.5%;font-size:13px;font-weight:400;line-height:150%;color:#999;font-family:sans-serif" align="center" valign="top">All Rights Reserved &copy; 2022 Onconavigator.in.<br>Unsubscribe - Contact us - Terms of Use -<a style="text-decoration:underline;color:#999;font-family:sans-serif;font-size:13px;font-weight:400;line-height:150%" target="_blank">Privacy Policy | Disclaimer</a></td></tr></tbody></table></body></html>`,
          },
        },
        Subject: {
          Charset: "UTF-8",
          Data: `Hello, ${name}! Please Change your Head & Neck Application Password.`,
        },
      },
    };
    // console.log(accessToken.accessToken)
    return AWS_SES.sendEmail(params).promise();
  }

  // Password change confirmation link

  async passwordChangeConfirmationLink(recipientEmail, name) {
    let params = {
      Source: "Head & Neck <rahul.k@riverroute.in>",
      Destination: {
        ToAddresses: [`${recipientEmail}`],
      },
      ReplyToAddresses: [],
      Message: {
        Body: {
          Html: {
            Charset: "UTF-8",
            Data: `Hi, ${name} <br>Your password has been successfully changes. If the password is not changed by you. Please contact your Administration.`,
          },
        },
        Subject: {
          Charset: "UTF-8",
          Data: `Hello, ${name}! Please Change your Application Password`,
        },
      },
    };
    return AWS_SES.sendEmail(params).promise();
  }

  // Send Email Confirmation Request Access to User for PDF

  async pdfResourceRequestConfirmation(recipientEmail, pdfName, name) {
    let params = {
      Source: "Head & Neck <rahul.k@riverroute.in>",
      Destination: {
        ToAddresses: [`${recipientEmail}`],
      },
      ReplyToAddresses: [],
      Message: {
        Body: {
          Html: {
            Charset: "UTF-8",
            Data: `Dear, ${name} <br>Thank you for sending us a request for the resource ${pdfName}. Please allow us 24hrs for a response. We would be reaching out to you at the earliest with the relevant information. <br/>We would love to hear from you! If there is anything that we can do to make your experience better, please let us know and email us here (email@email.com)`,
          },
        },
        Subject: {
          Charset: "UTF-8",
          Data: `Resource Request: ${pdfName}`,
        },
      },
    };
    return AWS_SES.sendEmail(params).promise();
  }

  // Send Email Confirmation Request Access to User for Pdf
  async pdfResourceGrantConfirmation(recipientEmail, pdfName, name) {
    let params = {
      Source: "Head & Neck <rahul.k@riverroute.in>",
      Destination: {
        ToAddresses: [`${recipientEmail}`],
      },
      ReplyToAddresses: [],
      Message: {
        Body: {
          Html: {
            Charset: "UTF-8",
            Data: `Dear, ${name} <br>Access Granted for ${pdfName}.`,
          },
        },
        Subject: {
          Charset: "UTF-8",
          Data: `Resource Request: ${pdfName}`,
        },
      },
    };
    return AWS_SES.sendEmail(params).promise();
  }
}

module.exports = new AdminEmailService();

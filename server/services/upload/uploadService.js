const s3 = require("./awsS3credentialsService");
const multer = require("multer");
const multerS3 = require("multer-s3");
//Test Upload

const uploadAvatar = multer({
  storage: multerS3({
    s3: s3,
    bucket: "rocheapplication/avatar",
    acl: "public-read",
    //contentType: multerS3.AUTO_CONTENT_TYPE,
    metadata: function (req, file, cb) {
      cb(null, { fieldName: file.fieldname });
    },
    key: function (req, file, cb) {
      cb(null, Date.now().toString() + ".jpg");
    },
  }),
});

module.exports = uploadAvatar;

const demographyModel = require("../models/demographyModel");

class DemographyService {
  //Create Demography
  async submitDemography(data) {
    const user = await demographyModel.create(data);
    return user;
  }
}

module.exports = new DemographyService();

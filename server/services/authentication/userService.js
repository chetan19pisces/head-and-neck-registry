const UserModel = require("../../models/userModel");
const bcrypt = require("bcrypt");

class UserService {
  //Find User Already in Database or not
  async findUser(filter) {
    const user = await UserModel.findOne(filter);
    return user;
  }

  //Create user
  async createUser(data) {
    const user = await UserModel.create(data);
    return user;
  }

  //Show User Detail

  //Hash Password
  async hashPassword(password) {
    const salt = await bcrypt.genSalt(10);
    const hash = await bcrypt.hash(password, salt);
    return hash;
  }

  //Decrypt Password
  async verifyPass(passwordFromUser, passwordFromDB) {
    return await bcrypt.compare(passwordFromUser, passwordFromDB);
  }

  //Fetch Ip Address
  async fetchIP() {}

  // Update User Details
  async updateProfile(userId, data) {
    const user = await UserModel.findOneAndUpdate(userId, data);
    return user;
  }

  // Update Password from Dashboard
  async updateDashboardPassword(userId, data) {    
    const user = await UserModel.updateOne({ _id: userId }, { $set : { password: data} });     
    return user;
  }

  // Update User Details
  async updatePassword(email, data) {    
    const user = await UserModel.findOneAndUpdate({ email: email }, { $set : data });    
    return user;
  }

  //Update User Profile Image
  async updateUserProfileImage(_id, data) {  
    const user = await UserModel.findOneAndUpdate(_id, data);
    return user;
  }

  // Show User Detail
  async showUserDetail(filter) {
    const user = await UserModel.findOne(filter);
    return user;
  }

  // Show User Detail
  async updateUserDetail(filter, data) {        
    const user = await UserModel.updateOne(filter, data);
    return user;
  }

  //Find all Users
  async findAllUsers() {        
    const hcpusers = await UserModel.find();
    return hcpusers;
}
}

module.exports = new UserService();

const PdfAccessModel = require("../../models/accessRequest/PdfAccessModel");

class PdfAccessService {
  //Create Request
  async createRequest(data) {
    const accessRequest = await PdfAccessModel.create(data);
    return accessRequest;
  }
}

module.exports = new PdfAccessService();
const AccessRequestModel = require("../../models/accessRequest/AccessRequestModel");

class AccessServices {
  //Create Request
  async createRequest(data) {
    const accessRequest = await AccessRequestModel.create(data);
    return accessRequest;
  }
}

module.exports = new AccessServices();

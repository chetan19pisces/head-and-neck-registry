const mongoose = require("mongoose");

function DbConnect() {
  const DB_URL = process.env.ROCHE_DB_MONGO_URL;

  mongoose.connect(DB_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    // useFindAndModify: false,
  });

  const db = mongoose.connection;
  db.on("error", console.error.bind(console, "Connection Error due to: "));
  db.once("open", () => {
    console.log(`DB connected`);
  });
}

module.exports = DbConnect;

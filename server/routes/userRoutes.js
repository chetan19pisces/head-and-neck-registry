const userController = require("../controllers/Users/userController");
const demographyController = require("../controllers/Users/demographyController");

const router = require("express").Router();

const authMiddleware = require("../middlewares/authMiddleware");
const initialpresentationController = require("../controllers/Users/initialpresentationController");

//
router.post("/", userController.registerUser);

//Login
router.post("/login", userController.loginUser);

//Logout
router.post("/logout", authMiddleware, userController.logout);

//Activate User
router.get("/activate/:accessToken", userController.activate);

//Dashboard top blocks data
router.get("/show-users", userController.totalUsers);

//Add All Patient Details
router.post("/demography", demographyController.submitDemography);
router.post("/initial-presentation/:code", initialpresentationController.submitInitialPresentation);

//Forgot Password
router.post("/forgot-password", userController.forgotPassword);

//Change Password
router.post("/change-password/:userId", userController.changePassword);
router.post("/change-email-password/:accessToken/:email", userController.changeEmailPassword);

//Upload Avatar
router.post("/upload-avatar", userController.uploadAvatar);

router.get("/show-user/:_id", userController.showUserDetail);
router.post("/update-profile/:_id", userController.updateUserDetail);

router.get("/refresh", userController.refresh);

//Roche users
router.post("/send-code/:code", userController.getToken);

//AWS Bucket
router.get('/bucket', userController.getBucketURL);
router.put('/update-profile/:_id', userController.updateProfileImage);

module.exports = router;

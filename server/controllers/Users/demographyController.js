const demographyService = require("../../services/demographyService");

class DemographyController {
  async submitDemography(req, res){
    try{
      const body = req.body.finalData;
      const demography = await demographyService.submitDemography(body);
      if(demography){
        return res.status(201).json({message: "Success - Demography Submitted"});
      }else{
        return res.status(500).json({message: "Error - Demography not Submitted"});
      }
    }catch(err){
      console.log(err.message)
    }
  }
}

module.exports = new DemographyController();

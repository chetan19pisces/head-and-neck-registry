const User = require("../../models/userModel");
const tokenService = require("../../services/authentication/tokenService");
const userService = require("../../services/authentication/userService");
const emailService = require("../../services/email/emailService");
const uploadAvatar = require("../../services/upload/uploadService");
const UserDto = require("../../dtos/user-dto");
const generateUploadURL = require("../../s3");
const qs = require("qs");
const axios = require("axios");
const jwt = require("jsonwebtoken");

let access_token;
let refresh_token;
let decoded;
let userId;
let rocheuser;
let user;

class UserController {
  //Register User
  async registerUser(req, res) {
    //Fetch User Parameters
    const {
      name,
      email,
      password,
      phone,
      city,
      state,
      country,
      designation,
      speciality,
      hopsital,      
      // ipAddress,
    } = req.body.registerDetails;

    //Check user Already Exists or not
    try {
      const user = await userService.findUser({ email: email });
      //Hash password

      const hashedPassword = await userService.hashPassword(
        password,
      );

      //If not exists Add New User
      if (!user) {
        let user = await userService.createUser({
          name: name,
          email: email,
          password: hashedPassword,
          phone: phone,
          city: city,
          state: state,
          country: country,          
          speciality: speciality,
          hopsital: hopsital,          
          role: "admin",
          ipAddress: req.ip,               
        });

        const tokens = await tokenService.generateToken({
          _id: user._id,
        });

        //Send Email
        // await emailService.sendEmailForAccountActivation(
        //   email,
        //   name,
        //   tokens.accessToken
        // );

        let accessToken = tokens.accessToken;

        res.json({
          user,
          accessToken,
          message: "Successfully Registered",
        });
      } else {
        return res.json({ message: "Already Registered" });
      }
    } catch (err) {
      console.log(err);
      return res.status(500).json({ message: err.message });
    }

    //If Exists Redirect to Login page
  }

  //Login User
  async loginUser(req, res) {
    //Fetch User Parameters
    const { email, password } = req.body;

    //Check user Exists or not
    const user = await userService.findUser({ email: email });

    //if Exists Match password and Email
    if (user) {
      //Match Password
      const verifyPass = await userService.verifyPass(password, user.password);

      if (verifyPass) {
        const { accessToken, refreshToken } = await tokenService.generateToken({
          _id: user._id,
        });

        await tokenService.storeRefreshToken(refreshToken, user._id);

        res.cookie("refreshToken", refreshToken, {
          maxAge: 1000 * 60 * 60 * 24 * 30,
          httpOnly: true,
          secure: true,
        });

        res.cookie("accessToken", accessToken, {
          maxAge: 1000 * 60 * 60 * 24 * 30,
          httpOnly: true,
          secure: true,
        });

        //Send Response
        const userDto = new UserDto(user);
        res.json({
          message: "User Found",
          user: userDto,
          accessToken,
          refreshToken,
          auth: true,
          status: 200,
        });
      } else {
        res.json({ message: "The Password you have entered is wrong" });
      }
    } else {
      res.json({
        message:
          "Email that you have enetered is not registered. Kindly Contact the Administrator",
      });
    }
    //const verifyEmailPass = await userService//If Not exists Show Error Message
  }

  // Activate Account

  async activate(req, res) {
    try {
      //Fetch data from URL
      const { accessToken } = req.params;

      //Check if URL is Authenticated or not
      const tokenVerify = await tokenService.verifyAccessToken(accessToken);

      //If Authenticated fetch user id
      const userIdFromToken = tokenVerify.userId;

      //Check Activation TRUE or False
      const user = await userService.findUser({ _id: userIdFromToken });
      if (!user) {
        return res.status(404).json({ message: "User not found!" });
      }

      if (user.activated === true) {
        //If True show already activated
        res.status(200).json({ message: "User Already Activated" });
      } else {
        //If False Make it true
        user.activated = true;
        user.save();
        res.status(200).json({ message: "User Activated" });
      }
    } catch (err) {
      res.status(401).json({ message: "Invalid Token", error: err.message });
    }
  }

  // Forgot Password
  async forgotPassword(req, res) {
    //Fetch Email ID from Body
    const { email } = req.body;

    //Check Email ID is available or not
    const user = await userService.findUser({ email: email });

    //If not - Give response Email id is not present
    if (!user) {
      return res.status(404).json({ message: "User not found!" });
    } else {
      //Generate Token
      const accessToken = await tokenService.generateToken({
        userId: user._id,
      });

      //If Available send Forgot password Link
      await emailService.sendForgotPasswordLink(email, user.name, accessToken);
    }

    res
      .status(200)
      .json({ message: "Please check your Email to change your password" });
  }

  // Change Password
  async changePassword(req, res) {
    try {
      // Fetch Token
      const { userId } = req.params;
      
      // Fetch updated Password from Post
      const { password } = req.body;      
      // Hash the Password
      const hashedPassword = await userService.hashPassword(password);      
      
      // Update the password to the respective User ID
      const passwordStatus = await userService.updateDashboardPassword(userId, hashedPassword);
      if(passwordStatus){
        // Show Message and send password change email to user
        res.status(200).json({ message: "Your Password has been changed" });
      }
    } catch (err) {
      res.status(401).json({ message: err.message });
    }
  }

  // Change Password
  async changeEmailPassword(req, res) {
    try {
      // Fetch Token
      const { accessToken, email } = req.params;
      console.log("Acc Token: " + accessToken);
      // Verify Token
      const tokenVerify = await tokenService.verifyAccessToken(accessToken);

      // Fetch updated Password from Post
      const { password } = req.body;
      // Hash the Password
      const hashedPassword = await userService.hashPassword(password);
      // Update the password to the respective User ID
      const passwordStatus = await userService.updatePassword(
        email,
        {
          password: hashedPassword,
        }
      );

      // Show Message and send password change email to user
      res.status(200).json({ message: "Your Password has been changed" });
    } catch (err) {
      res.status(401).json({ message: "Invalid Token", error: err.message });
    }
  }

  // Upload Avatar
  async uploadAvatar(req, res) {
    try {
      const singleUpload = uploadAvatar.single("image");
      singleUpload(req, res, function (err) {
        if (err) {
          return res.json({
            success: false,
            errors: {
              title: "Image Upload Error",
              detail: err.message,
              error: err,
            },
          });
        }
        let updateAvatar = { avatar: req.file.location };

        res.status(200).json({ message: "Avatar Upload Successfully" });
      });
    } catch (err) {
      res
        .status(500)
        .json({ message: "Something went Wrong!", err: err.message });
    }
  }

  // Show User Details

  async showUserDetail(req, res) {
    try {
      const id = req.params;
      const userDetail = await userService.showUserDetail(id);
      if (userDetail) {
        return res.json({ userDetail });
      } else {
        return res.json({ message: "No User found" });
      }
    } catch (err) {
      return res.status(500).json({ message: err.message });
    }
  }

  // Update User Details
  async updateUserDetail(req, res) {
    // Update user
    const userId = req.params
    const userData = req.body;    
    // return;
    try {
      const user = await userService.findUser(userId);      
      if (!user) {
        res.status(404).json({ message: "User not found!" });
      } else {        
        const userDetails = await userService.updateUserDetail(
          userId,
          { $set: {name: userData.name, email: userData.email, phone: userData.phone, designation: userData.designation, city: userData.city, state: userData.state, country: userData.country, clinic: userData.clinic, speciality: userData.speciality}  }
        );              
        if (userDetails) {
            return res.json({ userDetails });
        }else{
            return res.json({ message: "No user found" });
        }
      }
    } catch (err) {
      return res.status(500).json({ message: err.message });
    }
  }


  async refresh(req, res) {
    // get refresh token from cookie
    const { refreshToken: refreshTokenFromCookie } = req.cookies;

    // check if token is valid
    let userData;
    try {
      userData = await tokenService.verifyRefreshToken(refreshTokenFromCookie);
    } catch (err) {
      return res.status(401).json({ message: "Invalid Token" });
    }
    // Check if token is in db
    try {
      const token = await tokenService.findRefreshToken(
        userData._id,
        refreshTokenFromCookie
      );

      if (!token) {
        return res.status(401).json({ message: "Invalid token", token: token });
      }
    } catch (err) {
      return res.status(500).json({ message: "Internal error", err: err });
    }
    // check if valid user
    const user = await userService.findUser({ _id: userData._id });
    if (!user) {
      return res.status(404).json({ message: "No user" });
    }
    // Generate new tokens
    const { refreshToken, accessToken } = await tokenService.generateToken({
      _id: userData._id,
    });

    // Update refresh token
    try {
      await tokenService.updateRefreshToken(userData._id, refreshToken);
    } catch (err) {
      return res.status(500).json({ message: "Internal error" });
    }
    // put in cookie
    res.cookie("refreshToken", refreshToken, {
      maxAge: 1000 * 60 * 60 * 24 * 30,
      httpOnly: true,
      secure: true,
    });

    res.cookie("accessToken", accessToken, {
      maxAge: 1000 * 60 * 60 * 24 * 30,
      httpOnly: true,
      secure: true,
    });
    // response

    const userDto = new UserDto(user);
    res.json({ user: userDto, auth: true });
  }

  //Logout

  async logout(req, res) {
    const { refreshToken } = req.cookies;
    // delete refresh token from db
    await tokenService.removeToken(refreshToken);
    // delete cookies
    res.clearCookie("refreshToken");
    res.clearCookie("accessToken");
    res.json({ user: null, auth: false });
  }

  //Roche Login
  async getToken(req, res) {
    const options = {
      method: "post",
      url: "https://wamua.roche.com/as/token.oauth2",
      data: qs.stringify({
        grant_type: "authorization_code",
        client_id: "RiverRoute1",
        client_secret:
          "8Y1U7Lwki0yOO0DgL099JQxO1ZqyXzlJUa687qqn3Lm9A60akiY2ECvDtgB8N1Ek",
        //redirect_uri: "https://test.onconavigator.in/auth/roche-login",
        redirect_uri: `${process.env.REACT_APP_DEFAULT_URL}/auth/roche-login`,
        code: req.params.code,
      }),
      headers: {
        "content-type": "application/x-www-form-urlencoded;charset=utf-8",
      },
      withCredentials: true,
    };
    try {
      const response = await axios(options);
      access_token = response.data.access_token;
      refresh_token = response.data.refresh_token;
      decoded = jwt.decode(access_token, { complete: true });

      try {
        user = await userService.findUser({ email: decoded.payload.email });
        if (!user) {
          user = await userService.createUser({
            name: decoded.payload.firstName + " " + decoded.payload.lastName,
            email: decoded.payload.email,
            country: decoded.payload.country,
            activated: true,
            role: "roche",
          });
          userId = user._id;
        }

        const { accessToken, refreshToken } = tokenService.generateTokens({
          _id: user._id,
        });

        console.log(accessToken, refreshToken);

        res.cookie("accessToken", accessToken, {
          maxAge: 1000 * 60 * 60 * 24 * 30,
          httpOnly: true,
          secure: true,
        });

        res.cookie("refreshToken", refreshToken, {
          httpOnly: true,
          secure: true,
          maxAge: 1000 * 60 * 60 * 24 * 30,
        });

        await tokenService.storeRefreshToken(refreshToken, user._id);

        const userDto = new UserDto(user);
        res.json({ user: userDto, auth: true, accessToken, refreshToken });
      } catch (err) {
        console.log(err);
        return res.status(500).json({ message: "DB Error" });
      }

      // });
    } catch (err) {
      console.log({ message: err });
      //next();
    }
  }

  async getBucketURL(req, res) {
    try {
      const url = await generateUploadURL();
      return res.send({ url });
    } catch (err) {
      console.log(err);
    }
  }

  async updateProfileImage(req, res) {
    try {
      const _id = req.body.userId;
      const imageUrl = req.body.imageUrl;
      const userImage = await userService.updateUserProfileImage(
        { _id: _id },
        { $set: { avatar: imageUrl } }
      );
      if (userImage) {
        return res.json({ userImage });
      } else {
        return res.json({ message: "No Profile found" });
      }
    } catch (err) {
      return res.status(500).json({ message: err.message });
    }
  }

  async totalUsers(req, res) {
    try {
      const allUsers = await userService.findAllUsers();

      if (allUsers) {
        return res.json({ allUsers });
      } else {
        return res.json({ message: "No Users found" });
      }
    } catch (err) {
      return res.status(500).json({ message: err.message });
    }
  }
}

module.exports = new UserController();

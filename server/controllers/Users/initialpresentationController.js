const initialpresentationService = require("../../services/initialpresentationService");

class InitialPresentationController {
  async submitInitialPresentation(req, res){
    //Submit new or Update Entry
    const { code } = req.params;
    const body = req.body.finalData;    
    // return;
    try {
      const initial = await initialpresentationService.findInitalPresentation(code);      
      if (!initial) {
        // res.status(404).json({ message: "Data not found!" });
        const initialpresentation = await initialpresentationService.submitInitialPresentation(body);
        if(initialpresentation){
          return res.status(200).json({message: "Success - Initial Presentation Submitted"});
        }else{
          return res.status(500).json({message: "Error - Initial Presentation not Submitted"});
        }
      } else {        
        const initialDetails = await initialpresentationService.updateInitialPresentation(
          code,
          { $set: body }
        );                 
        if (initialDetails) {
         return res.json({ initialDetails, message: "Success - Initial Presentation Updated" });
        }else{
          return res.status(500).json({message: "Error - Initial Presentation not Updated"});
        }
      }
    } catch (err) {
      return res.status(500).json({ message: err.message });
    }
  }
}

module.exports = new InitialPresentationController();

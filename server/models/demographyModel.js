const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const demographySchema = new Schema(
  {
    age_at_diagnosis: { type: String, required: true },
    alcohol: { type: String, required: true },
    bmi: { type: Number, required: true },
    bsa: { type: Number, required: true },
    city: { type: String, required: true },
    co_morboditie: { type: String, required: true },
    code: { type: String, required: true },
    country: { type: String, required: true },
    date_of_birth: { type: String, required: true },
    date_of_diagnosis: { type: String, required: true },
    diet: { type: String, required: true },
    educational_qualification: { type: String, required: true },
    ethnicity: { type: String, required: true },
    has_cancer: { type: String, required: true },
    height: { type: String, required: true },
    hospital_id: { type: String, required: true },
    intake_type: { type: Array, required: false },
    number_of_pegs: { type: String, required: false },
    number_of_years_Chewing: { type: String, required: false },
    number_of_years_Smoking: { type: String, required: false },
    other_ethnicity: { type: String, required: false },
    other_qualification: { type: String, required: false },
    patient_initial: { type: String, required: true },
    patient_name: { type: String, required: true },
    praffin_blocks: { type: String, required: true },    
    presenting_symptom_Brother: { type: String, required: false },
    presenting_symptom_Father: { type: String, required: false },
    presenting_symptom_Mother: { type: String, required: false },
    presenting_symptom_Other: { type: String, required: false },
    presenting_symptom_Sister: { type: String, required: false },
    relative_age_Brother: { type: String, required: false },
    relative_age_Father: { type: String, required: false },
    relative_age_Mother: { type: String, required: false },
    relative_age_Other: { type: String, required: false },
    relative_age_Sister: { type: String, required: false },
    tobacco: { type: String, required: true },
    type_of_cancer_Brother: { type: String, required: false },
    type_of_cancer_Father: { type: String, required: false },
    type_of_cancer_Mother: { type: String, required: false },
    type_of_cancer_Other: { type: String, required: false },
    type_of_cancer_Sister: { type: String, required: false },
    weight: { type: String, required: true },
    which_relative: { type: Array, required: false },    
    flag: { type: Boolean, required: true, default: true}
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("Demography", demographySchema, "demography");
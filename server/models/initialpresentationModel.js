const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const initialpresentationSchema = new Schema(
  {
    at_diagnosis: {type: String, required: true},
    code: {type: String, required: true},
    cM: {type: String, required: true},
    cM_Based: {type: Array, required: true},
    cN: {type: String, required: true},
    cN_Based: {type: Array, required: true},
    cT: {type: String, required: true},
    cT_Based: {type: Array, required: true},
    first_treatment_given: {type: Array, required: true},
    genetics: {type: String, required: true},
    germline_testing: {type: String, required: true},
    laterality: {type: String, required: true},
    metastases: {type: String, required: true},
    other_genetics: {type: String, required: false},
    other_metastases_type: {type: String, required: false},
    presentation: {type: String, required: true},
    total_no_of_metastases: {type: String, required: true},
    types_of_metastases: {type: Array, required: true},    
    flag: { type: Boolean, required: true, default: true}
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("InitialPresentation", initialpresentationSchema, "initialpresentation");
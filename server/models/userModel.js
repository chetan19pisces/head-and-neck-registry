const mongoose = require("mongoose");
const Schema = mongoose.Schema;

const userSchema = new Schema(
  {
    name: { type: String, required: true },
    email: { type: String, required: true, unique: true },
    phone: { type: String, required: false },
    password: { type: String, required: false },
    city: { type: String, required: false },
    state: { type: String, required: false },
    country: { type: String, required: false },
    speciality: { type: String, required: false },
    hopsital: { type: String, required: false },    
    avatar: {
      type: String, required: false, default:"https://icon-library.com/images/anonymous-avatar-icon/anonymous-avatar-icon-25.jpg",
    },
    ipAddress: { type: String, required: false },
    role: { type: String, required: true, default: "admin" },
    activated: { type: Boolean, required: false, default: true },
  },
  {
    timestamps: true,
  }
);

module.exports = mongoose.model("User", userSchema, "users");

//https://medium.com/@towfiqu/nodejs-password-encryption-with-bcrypt-8f78d78dc3e8

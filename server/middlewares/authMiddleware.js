const tokenService = require("../services/authentication/tokenService");

module.exports = async function (req, res, next) {
  try {
    accessToken = req.cookies.accessToken;
    // console.log("Access Token: " + accessToken + "</br>");

    if (accessToken == undefined) {
      accessToken = req.headers["x-access-token"];
    }
    // console.log("Access Token: " + accessToken + "</br>");
    //const { accessToken } = req.cookies;

    if (!accessToken) {
      throw new Error();
    }
    const userData = await tokenService.verifyAccessToken(accessToken);
    if (!userData) {
      throw new Error();
    }
    req.user = userData;
    next();
  } catch (err) {
    res.status(401).json({ message: "Invalid token" });
  }
};

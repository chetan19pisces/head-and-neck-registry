require("dotenv").config();
const express = require("express");
const app = express();
const userRoutes = require("./routes/userRoutes.js");
const cors = require("cors");
const DbConnect = require("./database");
const PORT = process.env.ROCHE_PORT || 5000;
const cookieParser = require("cookie-parser");
const bodyParser = require("body-parser");
const os = require("os");

//Connect MYSQL
DbConnect();

const corsOption = {
  origin: ["http://localhost:3000"],
  credentials: true,
};

app.use(cookieParser());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors(corsOption));
app.use(express.json());

//app.use(router);

app.use("/api/v1/users", userRoutes);

app.get("/", (req, res) => {
  console.log(os.browser());
  res.send(`Hello from Roche: ${PORT}`);
});

app.listen(PORT, () => console.log(`Hello From Roche from Port :  ${PORT}`));

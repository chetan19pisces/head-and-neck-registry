import { configureStore } from "@reduxjs/toolkit";
import authenticationSlice from "./authenticationSlice";

export const store = configureStore({
  reducer: {
    authenticationSlice,
  },
});
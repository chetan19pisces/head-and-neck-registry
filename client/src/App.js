import React, { lazy, Suspense } from "react";
import { useSelector } from "react-redux";
import {
  BrowserRouter as Router,
  Routes,
  Route,
  // useLocation,
  Outlet,  
} from "react-router-dom";
import { useLoadingWithRefresh } from "./hooks/useLoadingWithRefresh";
import UseAdminState from './adminState/useAdminState';
import LoadingSpinner from "./components/adminContent/LoadingSpinner";
// import AdminSingleDetail from "./components/adminPages/AdminSingleDetail";

// Admin
const AdminLogin = lazy(() =>
  import("./components/adminPages/authentication/AdminLogin")
);
const AdminRegister = lazy(() =>
  import("./components/adminPages/authentication/AdminRegister")
);
const AdminDashboard = lazy(() =>
  import("./components/adminPages/AdminDashboard")
);
const AdminDemography = lazy(() =>
  import("./components/adminPages/AdminDemography")
);
const AdminInitialPresentation = lazy(() =>
  import("./components/adminPages/AdminInitialPresentation")
);

const PageNotFound = lazy(() =>
  import("./components/adminPages/404-notfound")
);

function App() {  
  const { loading } = useLoadingWithRefresh();
  
  return loading ? (
    <LoadingSpinner />
  ) : (
    <UseAdminState>
      <Router>
        <Suspense fallback={<LoadingSpinner />}>
          <Routes>
            <Route element={<RequireAuth />}>
            <>
              <Route path="/register" element={<AdminRegister />} />
              <Route path="/login" element={<AdminLogin />} />
              <Route path="/" element={<AdminLogin />} />
              <Route path="/dashboard" element={<AdminDashboard />} />              
              <Route path="/demography" element={<AdminDemography />} />              
              <Route path="/initial-presentation/" element={<AdminInitialPresentation />} />              
              <Route path="/initial-presentation/:code" element={<AdminInitialPresentation />} />              
              <Route path="*" element={<PageNotFound />} />
            </>
            </Route>
          </Routes>
        </Suspense>
      </Router>
    </UseAdminState>
  );
}

function RequireAuth() {
  const { isAuth } = useSelector((state) => state.authenticationSlice);
  // let location = useLocation();
  if (!isAuth) {
    // return <Navigate to="//login" state={{ from: location }} />;
  }

  return <Outlet />;
}

export default App;

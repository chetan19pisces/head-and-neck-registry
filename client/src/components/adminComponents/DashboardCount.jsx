import React, { useEffect, useState } from "react";
import { Col, UncontrolledAlert } from "reactstrap";
import { Icon } from "./Component";
import { useSelector } from "react-redux";

export const LoginToast = () => {
  const { user } = useSelector((state) => state.authenticationSlice);
  const [loginToast, setLoginToast] = useState(false);
  setTimeout(() => {
    dismissToast();
  }, 5000);
  useEffect(() => {
    if(localStorage.getItem('loginToast') === 'false'){
      dismissToast()
    }else{
      showToast()
    }
  }, []);
  function showToast(){
    setLoginToast(true);
    localStorage.setItem('loginToast', 'true');
  }
  function dismissToast(){
    setLoginToast(false);
    localStorage.setItem('loginToast', 'false');
  }
  return (
    <>
      {loginToast === true && localStorage.getItem('loginToast') === 'true' ? (
        <Col xxl="12" sm="12" className="px-1">
          <UncontrolledAlert
            className="alert alert-pro alert-dismissible w-100"
            color="success"
            fade={true}
            zoom={true}
            onClick={dismissToast}
          >
            <div className="alert-text">
              <h6>Welcome {user.name}</h6>
              <p>
                Successfully Logged In{" "}
              </p>
            </div>
          </UncontrolledAlert>
        </Col>
      ) : null}
    </>
  );
};

export const AllUsers = () => {
  const [usercount, setUserCount] = useState(0);
  async function getAllUserCount() {
    await fetch(`${process.env.REACT_APP_DEFAULT_URL}/api/v1/users/show-users`, {
      method: "GET",
    })
      .then((response) => response.json())
      .then((res) => {
        setUserCount(res.allUsers.length);
      });
  }
  useEffect(() => {
    getAllUserCount();
  });
  return (
    <>
      <div className="data">
        <div className="data-group">
          <Icon className="mr-2" name="user-list-fill"></Icon>
          <div className="amount">{usercount}</div>
        </div>
      </div>
    </>
  );
};

export const HCPUsers = () => {
  const [hcpusercount, setUserCount] = useState(0);
  async function getHCPUserCount() {
    await fetch(`${process.env.REACT_APP_DEFAULT_URL}/api/v1/admin/hcp/roche`, {
      method: "GET",
    })
      .then((response) => response.json())
      .then((res) => {
        setUserCount(res.hcpUsers.length);
      });
  }
  useEffect(() => {
    getHCPUserCount();
  });
  return (
    <>
      <div className="data">
        <div className="data-group">
          <Icon className="mr-2" name="user-list"></Icon>
          <div className="amount">{hcpusercount}</div>
        </div>
      </div>
    </>
  );
};

export const Surveys = () => {
  const [allsurveys, setAllSurveys] = useState(0);
  async function getSurveys() {
    await fetch(
      `${process.env.REACT_APP_DEFAULT_URL}/api/v1/admin/users/survey`,
      {
        method: "GET",
      }
    )
      .then((response) => response.json())
      .then((res) => {
        setAllSurveys(res.totalSurveys.length);
      });
  }
  useEffect(() => {
    getSurveys();
  });
  return (
    <>
      <div className="data">
        <div className="data-group">
          <Icon className="mr-2" name="growth-fill"></Icon>
          <div className="amount">{allsurveys}</div>
        </div>
      </div>
    </>
  );
};

export const ActiveAccounts = () => {
  const [allusercount, setAllUserCount] = useState(0);
  async function getActiveAccounts() {
    await fetch(
      `${process.env.REACT_APP_DEFAULT_URL}/api/v1/admin/account/all`,
      {
        method: "GET",
      }
    )
      .then((response) => response.json())
      .then((res) => {
        setAllUserCount(res.allUsers.length);
      });
  }
  useEffect(() => {
    getActiveAccounts();
  });
  return (
    <>
      <div className="data">
        <div className="data-group">
          <Icon className="mr-2" name="user-check"></Icon>
          <div className="amount">{allusercount}</div>
        </div>
      </div>
    </>
  );
};

export const InActiveAccounts = () => {
  const [allusercount, setAllUserCount] = useState(0);
  async function getInActiveAccounts() {
    await fetch(
      `${process.env.REACT_APP_DEFAULT_URL}/api/v1/admin/account/inactive`,
      {
        method: "GET",
      }
    )
      .then((response) => response.json())
      .then((res) => {
        setAllUserCount(res.inactiveUsers.length);
      });
  }
  useEffect(() => {
    getInActiveAccounts();
  });
  return (
    <>
      <div className="data">
        <div className="data-group">
          <Icon className="mr-2" name="user-cross"></Icon>
          <div className="amount">{allusercount}</div>
        </div>
      </div>
    </>
  );
};

export const ArticlesPublished = () => {
  const [allarticlescount, setAllUserCount] = useState(0);
  async function getArticlesPublished() {
    await fetch(
      `${process.env.REACT_APP_DEFAULT_URL}/api/v1/users/show-articles`,
      {
        method: "GET",
      }
    )
      .then((response) => response.json())
      .then((res) => {        
        setAllUserCount(res.article.length);
      });
  }
  useEffect(() => {
    getArticlesPublished();
  });
  return (
    <>
      <div className="data">
        <div className="data-group">
          <Icon className="mr-2" name="article"></Icon>
          <div className="amount">{allarticlescount}</div>
        </div>
      </div>
    </>
  );
};

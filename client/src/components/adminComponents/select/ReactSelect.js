import React from "react";
import Select from "react-select";
import { FormGroup } from "reactstrap";

export const RSelect = ({ ...props }) => {
  return (
    <FormGroup>
      <div className="form-label-select">
        <label className="form-label" htmlFor={props.htmlfor}>
          {props.text}
        </label>
      </div>
      <div className="form-control-select">
        <select
          type={props.type}
          name={props.name}
          id={props.id}
          options={props.options}
          placeholder={props.placeholder}
          htmlFor={props.htmlfor}
          onChange={props.onChange}
          className="form-control form-select py-1"
          classNamePrefix="react-select"
        >
          {props.options.map((option, index) => (
            <option key={index} value={option.value}>
              {option.label}
            </option>
          ))}
        </select>
        {props.label && <span className="invalid">{props.label}</span>}
      </div>
    </FormGroup>
  );
};
export const MultiSelect = ({ ...props }) => {
  return (
    <FormGroup>
      <div className="form-label-select">
        <label className="form-label" htmlFor={props.htmlfor}>
          {props.text}
        </label>
      </div>
      <div className="form-control-select">
        <Select
          // type={props.type}
          // name={props.name}
          // id={props.id}
          options={props.options}
          // placeholder={props.placeholder}
          // htmlFor={props.htmlfor}
          onChange={props.onChange}
          className=""
          // classNamePrefix="react-select"
          isMulti
          isClearable
        >
          {props.options.map((option, index) => (
            <option key={index} value={option.value}>
              {option.label}
            </option>
          ))}
        </Select>
        {props.label && <span className="invalid">{props.label}</span>}
      </div>
    </FormGroup>
  );
};
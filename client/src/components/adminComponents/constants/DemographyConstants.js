export const praffinBlocks = [        
    { value: "Yes", label: "Yes" },
    { value: "No", label: "No" },
    { value: "Available", label: "Available" },    
];
export const hasCancer = [        
    { value: "Yes", label: "Yes" },
    { value: "No", label: "No" },    
];
export const yes_no = [        
    { value: "", label: "Please Select" },
    { value: "Yes", label: "Yes" },
    { value: "No", label: "No" },    
];
export const educational_qualification = [        
    { value: "", label: "Please Select" },
    { value: "High School", label: "High School" },
    { value: "Senior Secondary", label: "Senior Secondary" },
    { value: "Under Graduate", label: "Under Graduate" },
    { value: "Graduate", label: "Graduate" },
    { value: "Other", label: "Other" },
];
export const patient_ethnicity = [        
    { value: "", label: "Please Select" },
    { value: "Hindu", label: "Hindu" },
    { value: "Muslim", label: "Muslim" },
    { value: "Christian", label: "Christian" },
    { value: "Parsi", label: "Parsi" },
    { value: "Other", label: "Other" },
];
export const patient_relative = [        
    { value: "", label: "Please Select" },
    { value: "Mother", label: "Mother" },
    { value: "Father", label: "Father" },
    { value: "Brother", label: "Brother" },
    { value: "Sister", label: "Sister" },
    { value: "Other", label: "Other" },
];
export const type_of_cancers = [        
    { value: "", label: "Please Select" },
    { value: "Oral Cavity", label: "Oral Cavity" },
    { value: "Naso Pharynx", label: "Naso Pharynx" },
    { value: "Oro Pharynx", label: "Oro Pharynx" },
    { value: "Hypo Pharynx", label: "Hypo Pharynx" },
    { value: "Larynx", label: "Larynx" },
    { value: "Para Nasal", label: "Para Nasal" },
    { value: "Suavary Gland", label: "Suavary Gland" },
    { value: "Thyroid Gland", label: "Thyroid Gland" },
];
export const presenting_symptoms = [        
    { value: "Screen Detected", label: "Screen Detected" },
    { value: "Symptom Detected", label: "Symptom Detected" },    
];
export const co_morbodities = [        
    { value: "Hypertension", label: "Hypertension" },
    { value: "DM", label: "DM" },    
    { value: "Other", label: "Other" },    
];
export const intake_types = [        
    { value: "", label: "Please Select" },
    { value: "Smoking", label: "Smoking" },
    { value: "Chewing", label: "Chewing" },        
];
export const diets = [        
    { value: "", label: "Please Select" },
    { value: "Veg", label: "Veg" },
    { value: "Non-Veg", label: "Non-Veg" },
    { value: "Vegan", label: "Vegan" },
    { value: "Both Veg & Non-Veg", label: "Both Veg & Non-Veg" },
];
export const presentation = [            
    { value: "", label: "Please Select" },
    { value: "Screen Detected", label: "Screen Detected" },
    { value: "Symptomatic", label: "Symptomatic" },    
];
export const at_diagnosis = [            
    { value: "", label: "Please Select" },
    { value: "Early", label: "Early" },
    { value: "Locally Advanced", label: "Locally Advanced" },    
];
export const laterality = [            
    { value: "", label: "Please Select" },
    { value: "Left", label: "Left" },
    { value: "Right", label: "Right" },    
    { value: "Bilateral", label: "Bilateral" },    
];
export const cT = [            
    { value: "", label: "Please Select" },
    { value: "X", label: "X" },
    { value: "1", label: "1" },    
    { value: "2", label: "2" },    
    { value: "3", label: "3" },    
    { value: "4", label: "4" },    
];
export const cN = [            
    { value: "", label: "Please Select" },
    { value: "X", label: "X" },
    { value: "0", label: "0" },    
    { value: "1", label: "1" },    
    { value: "2", label: "2" },    
    { value: "3", label: "3" },    
];
export const cM = [            
    { value: "", label: "Please Select" },
    { value: "X", label: "X" },
    { value: "0", label: "0" },        
    { value: "1", label: "1" },    
    { value: "2", label: "2" },    
];
export const cT_Based = [            
    // { value: "", label: "Please Select" },
    { value: "Clinical", label: "Clinical" },
    { value: "MRI", label: "MRI" },    
    { value: "Ultrasound", label: "Ultrasound" },     
];
export const cN_Based = [            
    // { value: "", label: "Please Select" },
    { value: "Clinical", label: "Clinical" },
    { value: "MRI", label: "MRI" },    
    { value: "Ultrasound", label: "Ultrasound" },
];
export const cM_Based = [            
    // { value: "", label: "Please Select" },
    { value: "Clinical", label: "Clinical" },
    { value: "MRI", label: "MRI" },    
    { value: "Ultrasound", label: "Ultrasound" },
];
export const metastases = [                
    { value: "Yes", label: "Yes" },    
    { value: "No", label: "No" },
];
export const total_no_of_metastases = [                
    { value: "Less than 5", label: "Less than 5" },    
    { value: "More than 5", label: "More than 5" },
];
export const types_of_metastases = [                
    { value: "Liver", label: "Liver" },    
    { value: "Lung", label: "Lung" },
    { value: "Bone", label: "Bone" },
    { value: "Brain", label: "Brain" },
    { value: "Other", label: "Other" },
];
export const first_treatment_given = [                
    { value: "Surgery", label: "Surgery" },    
    { value: "Chemotherapy", label: "Chemotherapy" },
    { value: "Targeted Therapy", label: "Targeted Therapy" },
    { value: "Immunotherapy", label: "Immunotherapy" },
    { value: "Hormone Therapy", label: "Hormone Therapy" },
    { value: "Radiotherapy", label: "Radiotherapy" },
    { value: "Trial", label: "Trial" },
    { value: "Alternative Therapy", label: "Alternative Therapy" },
    { value: "Declined All Therapies", label: "Declined All Therapies" },
];
export const germline_testing = [                
    { value: "Yes", label: "Yes" },    
    { value: "No", label: "No" },
];
export const genetics = [                
    { value: "", label: "Please Select" },    
    { value: "BRCA 1", label: "BRCA 1" },    
    { value: "BRCA 2", label: "BRCA 2" },
    { value: "PALB2", label: "PALB2" },
    { value: "ATM", label: "ATM" },
    { value: "CHEK 2", label: "CHEK 2" },
    { value: "p53", label: "p53" },
    { value: "Other", label: "Other" },
];
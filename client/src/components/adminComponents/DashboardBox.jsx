import React from "react";
import { Card } from "reactstrap";

const DashboardBox = ({ title, count }) => {
  return (
    <Card>
      <div className="nk-ecwg nk-ecwg6">
        <div className="card-inner">
          <div className="card-title-group">
            <div className="card-title">
              <h6 className="titles">{title}</h6>
            </div>
          </div>
          <div className="data">
            <div className="data-group">
              <div className="amount">{count}</div>              
            </div>            
          </div>
        </div>
      </div>
    </Card>
  );
};

export default DashboardBox;

import React, { useState } from "react";
import { FormGroup } from "reactstrap";
import DatePicker from 'react-datepicker'

const DatePickerInput = ({ label, text, type, name, placeholder, size, selected, id, htmlfor, onChange }) => {
  const [focus, setFocus] = useState(false);
  return (
    <>      
      <FormGroup>
        <div className="form-label-group">
          <label className="form-label" htmlFor={htmlfor}>
            {text}
          </label>
        </div>
        <div className="form-control-wrap">                                                  
          {/* <input type={type} name={name} id={id} placeholder={placeholder} htmlFor={htmlfor} className={`form-control-lg form-control ${size ? `form-control-${size}` : ""}`} onChange={onChange}   />      */}
          <DatePicker selected={selected} startDate={selected} onChange={onChange} dateFormat="dd-MM-yyyy" className={`form-control-lg form-control date-picker ${size ? `form-control-${size}` : ""}`} placeholderText={placeholder} maxDate={new Date()} />
          {label && (
            <span className="invalid">
              {label}
            </span>
          )}
        </div>        
      </FormGroup>      
    </>
  );
};

export default DatePickerInput;

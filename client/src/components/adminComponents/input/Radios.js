import React, { useState } from "react";
import { FormGroup } from "reactstrap";

const Inputs = ({ label, text, type, name, options, size, id, htmlfor, onChange }) => {
  const [focus, setFocus] = useState(false);
  return (
    <>      
      <FormGroup>
        <div className="form-label-group">
          <label className="form-label">
            {text}
          </label>
        </div>
        <div className="custom-control custom-radio w-100">     
          {
            options.map((option, index) => (
              <span className="mr-1 w-100 minus"><input type={type} name={name} id={id+"_"+index} className={`custom-control-input form-control`} onChange={onChange} value={option.label}   /> 
              <label className="custom-control-label" id={id+"_"+index} htmlFor={id+"_"+index}>{option.label}</label>
              </span>
            ))
          }                                      
                           
          {label && (
            <span className="invalid">
              {label}
            </span>
          )}
        </div>        
      </FormGroup>      
    </>
  );
};

export default Inputs;

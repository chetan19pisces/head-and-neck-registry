import React from "react";
import { FormGroup } from "reactstrap";

const Inputs = ({ label, text, type, name, placeholder, size, id, htmlfor, onChange, disable, value }) => {  
  return (
    <>      
      <FormGroup>
        <div className="form-label-group">
          <label className="form-label" htmlFor={htmlfor}>
            {text}
          </label>
        </div>
        <div className="form-control-wrap">                                                  
          <input type={type} name={name} id={id} placeholder={placeholder} htmlFor={htmlfor} className={`form-control-lg form-control ${size ? `form-control-${size}` : ""}`} onChange={onChange} disabled={disable} value={value}  />                  
          {label && (
            <span className="invalid">
              {label}
            </span>
          )}
        </div>        
      </FormGroup>      
    </>
  );
};

export default Inputs;

import classNames from "classnames";
import React, { useEffect, useLayoutEffect, useState } from "react";
import { Link } from "react-router-dom";
import { Button, Icon } from "../adminComponents/Component";
import Footer from "../adminLayout/Layouts/Footer";
import Header from "../adminLayout/Layouts/Header";
import HeaderSearch from "../adminLayout/Layouts/HeaderSearch";
import PageContainer from "../adminLayout/Layouts/PageContainer";
import Sidebar from "../adminLayout/Layouts/Sidebar";

const Error404NotFound = () => {
  const [mobileView, setMobileView] = useState();
  const [visibility, setVisibility] = useState(false);
  const [themeState] = useState({
    main: "default",
    sidebar: "white",
    header: "white",
    skin: "light",
  });

  useEffect(() => {
    document.body.className = `nk-body bg-lighter npc-default has-sidebar no-touch nk-nio-theme ${
      themeState.skin === "dark" ? "dark-mode" : ""
    }`;
  }, [themeState.skin]);

  useEffect(() => {
    viewChange();
  }, []);

  // Stops scrolling on overlay
  useLayoutEffect(() => {
    if (visibility) {
      document.body.style.overflow = "hidden";
      document.body.style.height = "100%";
    }
    if (!visibility) {
      document.body.style.overflow = "auto";
      document.body.style.height = "auto";
    }
  }, [visibility]);

  // function to toggle sidebar
  const toggleSidebar = (e) => {
    e.preventDefault();
    if (visibility === false) {
      setVisibility(true);
    } else {
      setVisibility(false);
    }
  };

  // function to change the design view under 1200 px
  const viewChange = () => {
    if (window.innerWidth < 1200) {
      setMobileView(true);
    } else {
      setMobileView(false);
      setVisibility(false);
    }
  };
  window.addEventListener("load", viewChange);
  window.addEventListener("resize", viewChange);

  const sidebarClass = classNames({
    "nk-sidebar-mobile": mobileView,
    "nk-sidebar-active": visibility && mobileView,
  });
  return (
    <PageContainer>
      <div className="nk-app-root">
        <div className="nk-main">
          <Sidebar
            sidebarToggle={toggleSidebar}
            fixed
            mobileView={mobileView}
            theme={themeState.sidebar}
            className={sidebarClass}
          />
          {visibility && mobileView && <div className="nk-sidebar-overlay" onClick={toggleSidebar}></div>}
          <div className="nk-wrap">
            <Header sidebarToggle={toggleSidebar} fixed setVisibility={setVisibility} theme={themeState.header} />
      <div className="nk-block nk-block-middle wide-xs mx-auto">
        <div className="nk-block-content nk-error-ld text-center">
          <h1 className="nk-error-head">404</h1>
          <h3 className="nk-error-title">Oops! Page Not Found?</h3>
          <p className="nk-error-text">
            We are very sorry for inconvenience. It looks like you’re try to access a page that either has been deleted
            or never existed.
          </p>
          <Link to={`${process.env.PUBLIC_URL}/dashboard`}>
            <Button color="primary" size="lg" className="mt-2">
              <Icon name="home" className="mr-2"></Icon> Back To Home
            </Button>
          </Link>
        </div>
      </div>
      <Footer />
          </div>
        </div>
      </div>
    </PageContainer>
  );
};
export default Error404NotFound;

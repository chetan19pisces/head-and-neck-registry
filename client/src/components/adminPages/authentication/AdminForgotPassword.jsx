import React, { useState } from "react";
import {Block, BlockContent, BlockDes, BlockHead, BlockTitle, Button, Icon, PreviewCard} from "../../adminComponents/Component";
import { Form, FormGroup, Spinner, Alert } from "reactstrap";
import PageContainer from "../../adminLayout/Layouts/PageContainer";
import AdminFooter from "./AdminFooter";
import { Link } from "react-router-dom";
import Logo from "../../../assets/admin/icons/hnn-logo.png";
import { forgotPasswordEmail } from '../../../http/index'

const AdminForgotPassword = () => {
  const [email, setEmail] = useState('');
  const [showResponseEmail, setShowResponseEmail] = useState("");
  const [showResponse, setShowResponse] = useState("");
  const [loading, setLoading] = useState(false);
  async function onFormSubmit(e) {
    e.preventDefault();
    //Validation
    const regex =
      /^(([^<>()[\].,;:\s@"]+(\.[^<>()[\].,;:\s@"]+)*)|(".+"))@(([^<>()[\].,;:\s@"]+\.)+[^<>()[\].,;:\s@"]{2,})$/i;
    if (email.length === 0) {
      setShowResponseEmail("Please enter your Email address");
      setShowResponse('')
      return;
    } else if (regex.test(email) === false) {
      setShowResponseEmail("Please enter valid Email address");
      setShowResponse('')
      return;
    } else {
      setShowResponseEmail("");
    }    

    //Check Credentials
    setLoading(true);
    try {
      const { data } = await forgotPasswordEmail({ email });
      setShowResponseEmail("")
      setShowResponse(data.message);
      if (data.status === 200) {
        
        //localStorage.setItem("userInfo", JSON.stringify(data.token));
      }
    } catch (err) {
      console.log("Hello: " + err);
    } finally {
      setLoading(false);
    }
  }

  // const { errors, register, handleSubmit } = useForm();

  return (
    <React.Fragment>
      <PageContainer>
        <Block className="nk-block-middle nk-auth-body  wide-xs">
          <div className="brand-logo pb-4 text-center">
            <Link to={process.env.PUBLIC_URL + "/"} className="logo-link">
              {/* <img className="logo-light logo-img logo-img-lg" src="./assets/images/logo/hnn-logo.png" alt="logo" />               */}
              <img className="logo-img logo-img-lg" src={Logo} alt="logo" />              
            </Link>
          </div>

          <PreviewCard className="card-bordered" bodyclassName="card-inner-lg">
            <BlockHead>
              <BlockContent>
                <BlockTitle tag="h5">Reset password</BlockTitle>
                <BlockDes>
                  <p>If you forgot your password, well, then we’ll email you instructions to reset your password.</p>
                </BlockDes>
              </BlockContent>
            </BlockHead>            
            <Form className="is-alter">
              <FormGroup>
                <div className="form-label-group">
                  <label className="form-label" htmlFor="default-01">
                    Email
                  </label>
                </div>
                <div className="form-control-wrap">
                  <input
                    type="text"
                    id="default-01"
                    name="email"                                        
                    placeholder="Enter your email"
                    className="form-control-lg form-control"
                    onChange={(e) => setEmail(e.target.value)}
                  />  
                  {/* {setShowResponseEmail == null ? <></> : (<Alert color="danger" className="alert-icon"><Icon name="alert-circle" /> {setShowResponseEmail}</Alert>)}  */}
                  {showResponseEmail == '' ? <></> : <p className="showvalidationError alert alert-danger"><Icon name="alert-circle" /> {showResponseEmail}</p>}
                </div>
              </FormGroup>
              <FormGroup>
                <Button size="lg" className="btn-block" type="submit" color="primary" onClick={onFormSubmit}>
                  {loading ? <Spinner size="sm" color="light" /> : "Send Reset Link"}
                </Button>
              </FormGroup>
              {showResponse && <Alert color="success" className="alert-icon">
                {" "}
                <Icon name="alert-circle" /> {showResponse}{" "}
              </Alert>}              
              <div className="form-note-s2 text-center pt-0">
              <Link to={`${process.env.PUBLIC_URL}/login`}>
                <strong>Return to login</strong>
              </Link>
            </div>
            </Form>            
          </PreviewCard>
        </Block>
        <AdminFooter />
      </PageContainer>
    </React.Fragment>
  );
};
export default AdminForgotPassword;

import React, { useState } from "react";
import {Block, BlockContent, BlockDes, BlockHead, BlockTitle, Button, Icon, PreviewCard} from "../../adminComponents/Component";
import { Form, FormGroup, Spinner, Alert } from "reactstrap";
import PageContainer from "../../adminLayout/Layouts/PageContainer";
import AdminFooter from "./AdminFooter";
// import { useForm } from "react-hook-form";
import { Link, useNavigate } from "react-router-dom";
import Joi from "joi-browser";
import Logo from "../../../assets/admin/icons/hnn-logo.png";
import { login } from '../../../http/index'
import { setAuthentication } from '../../../store/authenticationSlice'
import { useDispatch } from "react-redux";

const AdminLogin = () => {
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const [loading, setLoading] = useState(false);
  const [passState, setPassState] = useState(false);
  const [errorVal, setError] = useState("");
  const [loginDetails, setLoginDetails] = useState({
    email: "",
    password: "",
  });

  const [errors, setErrors] = useState({});

  var schema = Joi.object().keys({
    email: Joi.string().min(3).max(50).required().label("Email"),
    password: Joi.string().min(6).max(50).required().label("Password"),
  });

  const onFormSubmit = async (e) => {    
    e.preventDefault();
    setLoading(true);
    // const loginName = "admin";
    // const pass = "onconavigator2022";
    if (loginDetails.email !== '' && loginDetails.password !== '') {
      setErrors({});
      localStorage.setItem("accessToken", "token");
      try {
        const { data } = await login({ email: loginDetails.email, password: loginDetails.password });
        setError(data.message);
        if (data.status === 200) {
          const userActivationStatus = data.user.activated;
  
          if (userActivationStatus === false) {
            setError(`User not Activated. Please contact Administrator.`);
          } else {
            //setShowResponse(data.message);
            dispatch(setAuthentication(data));
            navigate("/dashboard");
            // if(data.user.role === "admin"){
            //   navigate("/admin/dashboard");
            // }else{
            //   navigate("/dashboard");
            // }
          }
  
          //localStorage.setItem("userInfo", JSON.stringify(data.token));
        }
      } catch (err) {
        console.log("Hello: " + err);
      } finally {
        setLoading(false);
      }            
    }else if (loginDetails.email === '' || loginDetails.password === '') {
      const result = Joi.validate(loginDetails, schema, { abortEarly: false });
      const { error } = result;
      if (error === null) {
        setErrors({});        
      } else {
        setTimeout(() => {
          setError("Please Enter Email and Password");
          const errorData = {};
        for (let item of error.details) {
          const name = item.path[0];
          const message = item.message;
          errorData[name] = message;
        }
        console.log(errors);
        setErrors(errorData);
        setLoading(false);
        return errorData;          
        }, 1000);        
      }      
    }
    else {
      setTimeout(() => {
        setError("Cannot login with credentials");
        setLoading(false);
      }, 2000);
    }
  };

  // const { errors, register, handleSubmit } = useForm();

  return (
    <React.Fragment>
      <PageContainer>
        <Block className="nk-block-middle nk-auth-body  wide-xs">
          <div className="brand-logo pb-4 text-center">
            <Link to={process.env.PUBLIC_URL + "/"} className="logo-link">
              {/* <img className="logo-light logo-img logo-img-lg" src="./assets/images/logo/hnn-logo.png" alt="logo" />               */}
              <img className="logo-img logo-img-lg" src={Logo} alt="logo" />              
            </Link>
          </div>

          <PreviewCard className="card-bordered" bodyclassName="card-inner-lg">
            <BlockHead>
              <BlockContent>
                <h4>Sign-In</h4>
                <BlockDes>
                  <p>Access Head & Neck Admin using your email and password.</p>
                </BlockDes>
              </BlockContent>
            </BlockHead>
            {errorVal && (
              <div className="mb-3">
                <Alert color="danger" className="alert-icon">
                  {" "}
                  <Icon name="alert-circle" /> {errorVal}{" "}
                </Alert>
              </div>
            )}
            <Form className="is-alter">
              <FormGroup>
                <div className="form-label-group">
                  <label className="form-label" htmlFor="default-01">
                    Email
                  </label>
                </div>
                <div className="form-control-wrap">
                  <input
                    type="text"
                    id="default-01"
                    name="email"                                        
                    placeholder="Enter your email"
                    className="form-control-lg form-control"
                    onChange={(e) => setLoginDetails({ ...loginDetails, email: e.target.value })}
                  />     
                  {errors.email && (
                    <span className="invalid">
                      {errors.email}
                    </span>
                  )}
                </div>
              </FormGroup>
              <FormGroup>
                <div className="form-label-group">
                  <label className="form-label" htmlFor="password">
                    Passcode
                  </label>
                  <Link className="link link-primary link-sm" to={`${process.env.PUBLIC_URL}/forgot-password`}>
                    Forgot Code?
                  </Link>
                </div>
                <div className="form-control-wrap">
                  <a
                    href="#password"
                    onClick={(ev) => {
                      ev.preventDefault();
                      setPassState(!passState);
                    }}
                    className={`form-icon lg form-icon-right passcode-switch ${passState ? "is-hidden" : "is-shown"}`}
                  >
                    <Icon name="eye" className="passcode-icon icon-show"></Icon>

                    <Icon name="eye-off" className="passcode-icon icon-hide"></Icon>
                  </a>
                  <input
                    type={passState ? "text" : "password"}
                    id="password"
                    name="passcode"
                    defaultValue=""                    
                    placeholder="Enter your passcode"
                    className={`form-control-lg form-control ${passState ? "is-hidden" : "is-shown"}`}
                    onChange={(e) => setLoginDetails({ ...loginDetails, password: e.target.value })}
                  />  
                  {errors.password && (
                    <span className="invalid">
                      {errors.password}
                    </span>
                  )}                
                </div>
              </FormGroup>
              <FormGroup>
                <Button size="lg" className="btn-block" type="submit" color="primary" onClick={onFormSubmit}>
                  {loading ? <Spinner size="sm" color="light" /> : "Sign in"}
                </Button>
                <p className="mt-3 text-right">
                  Don't have an account? <Link className="link link-primary link-sm font_16" to={`${process.env.PUBLIC_URL}/register`}>
                  Register
                </Link>
                </p>
              </FormGroup>
            </Form>            
          </PreviewCard>
        </Block>
        <AdminFooter />
      </PageContainer>
    </React.Fragment>
  );
};
export default AdminLogin;

import React, { useState, useEffect } from "react";
import { Link, useParams } from 'react-router-dom'
import { updateEmailPassword } from "../../../http";
import { Button, FormGroup } from "reactstrap";
import { Block, BlockContent, BlockDes, BlockHead, BlockTitle, Icon, PreviewCard } from "../../adminComponents/Component";
import PageContainer from "../../adminLayout/Layouts/PageContainer";
import Logo from "../../../assets/admin/icons/hnn-logo.png";

const CreatePassword = () => {
  const { accessToken, email } = useParams();
  const [newpassword, setNewPassword] = useState("");
  const [confPassword, setConfPassword] = useState("");
  const [success, setSuccess] = useState();
  const [passNewState, setNewPassState] = useState(false);
  const [passConfState, setConfirmPassState] = useState(false);
  const [showResponsePassword, setShowResponsePassword] = useState("");
  const [showResponseConfirmPassword, setShowResponseConfirmPassword] =
    useState("");
  
  async function submit(e) {
    e.preventDefault();

    //Check password field null or not
    if (newpassword.length === 0) {
      setShowResponsePassword("Please enter your Password");
      return;
    } else {
      setShowResponsePassword("");
    }

    //Check confirm password field null or not
    if (confPassword.length === 0) {
      setShowResponseConfirmPassword("Please enter your Confirm Password");
      return;
    } else {
      setShowResponseConfirmPassword("");
    }

    //Check password and confirm password same or not
    if (confPassword != newpassword) {
      setShowResponseConfirmPassword("Password and Confirm Password not match");
      return;
    } else {
      setShowResponseConfirmPassword("");
    }

    //Update Password

    const { data } = await updateEmailPassword(accessToken, email, { newpassword });
    setSuccess(data.message)    
  }
  return (
    <React.Fragment>
      <PageContainer>
        <Block className="nk-block-middle nk-auth-body  wide-xs">
          <div className="brand-logo pb-4 text-center">
            <Link to={process.env.PUBLIC_URL + "/"} className="logo-link">
              {/* <img className="logo-light logo-img logo-img-lg" src="./assets/images/logo/hnn-logo.png" alt="logo" />               */}
              <img className="logo-img logo-img-lg" src={Logo} alt="logo" />              
            </Link>
          </div>

          <PreviewCard className="card-bordered" bodyclassName="card-inner-lg">
            <BlockHead>
              <BlockContent>
              <BlockTitle tag="h5">Reset password</BlockTitle>
                <BlockDes>
                  <p>If you forgot your password, well, then we’ll email you instructions to reset your password.</p>
                </BlockDes>
              </BlockContent>
            </BlockHead>     
            <FormGroup>
                <div className="form-label-group">
                  <label className="form-label" htmlFor="newpassword">
                    New Passcode
                  </label>                  
                </div>
                <div className="form-control-wrap">
                  <a
                    href="#newpassword"
                    onClick={(ev) => {
                      ev.preventDefault();
                      setNewPassState(!passNewState);
                    }}
                    className={`form-icon lg form-icon-right passcode-switch ${passNewState ? "is-hidden" : "is-shown"}`}
                  >
                    <Icon name="eye" className="passcode-icon icon-show"></Icon>

                    <Icon name="eye-off" className="passcode-icon icon-hide"></Icon>
                  </a>
                  <input
                    type="password"
                    class="form-control"
                    id="newpassword"
                    placeholder="Enter New Password"
                    value={newpassword}
                    name="newpassword"
                    onChange={(e) => setNewPassword(e.target.value)}
                  />
                  {setShowResponsePassword == null ? (
                    <></>
                  ) : (
                    showResponsePassword && <p className="showvalidationError alert alert-warning">
                      {showResponsePassword}
                    </p>
                  )}               
                </div>
              </FormGroup>
              <FormGroup>
                <div className="form-label-group">
                  <label className="form-label" htmlFor="confPassword">
                    Confirm Passcode
                  </label>                  
                </div>
                <div className="form-control-wrap">
                  <a
                    href="#confPassword"
                    onClick={(ev) => {
                      ev.preventDefault();
                      setConfirmPassState(!passConfState);
                    }}
                    className={`form-icon lg form-icon-right passcode-switch ${passConfState ? "is-hidden" : "is-shown"}`}
                  >
                    <Icon name="eye" className="passcode-icon icon-show"></Icon>

                    <Icon name="eye-off" className="passcode-icon icon-hide"></Icon>
                  </a>
                  <input
                    type="password"
                    class="form-control"
                    id="confirmpassword"
                    placeholder="Enter New Password"
                    value={confPassword}
                    name="confPassword"
                    onChange={(e) => setConfPassword(e.target.value)}
                  />
                  {setShowResponseConfirmPassword == null ? (
                    <></>
                  ) : (
                    showResponseConfirmPassword && <p className="showvalidationError alert alert-warning">
                      {showResponseConfirmPassword}
                    </p>
                  )}              
                </div>
              </FormGroup>
              <FormGroup>
                <Button size="lg" className="btn-block" type="submit" color="primary" onClick={submit}>Change Password</Button>
              </FormGroup>
            </PreviewCard>
          </Block>
        </PageContainer>
      </React.Fragment>
  );
};

export default CreatePassword;

import React, { useState } from "react";
import {Block, BlockContent, BlockHead, Button, Icon, PreviewCard} from "../../adminComponents/Component";
import { Form, FormGroup, Spinner, Alert, Row, Col } from "reactstrap";
import PageContainer from "../../adminLayout/Layouts/PageContainer";
import AdminFooter from "./AdminFooter";
import { Link } from "react-router-dom";
import Joi from "joi-browser";
import Logo from "../../../assets/admin/icons/hnn-logo.png";
import { register } from '../../../http/index'
import Inputs from "../../adminComponents/input/Inputs";
import { RSelect } from "../../adminComponents/select/ReactSelect";

const AdminRegister = () => {
  const [loading, setLoading] = useState(false);  
  const [errorVal, setError] = useState("");
  const filterStatus = [        
    { value: "", label: "Please Select" },
    { value: "Surgical Oncology", label: "Surgical Oncology" },
    { value: "Medical Oncology", label: "Medical Oncology" },
    { value: "Radiation Oncology", label: "Radiation Oncology" },
    { value: "Other", label: "Other" },
  ];
  const [registerDetails, setRegisterDetails] = useState({
    name: "", email: "", password: "", password_confirmation: "", city: "", state: "", country: "", speciality: "", hospital: "", avatar: "", phone: "", activated: false, role: "admin", ipAddress: "", plan: ""
  });

  const [errors, setErrors] = useState({});
  const [successMessage, setSuccessMessage] = useState("");

  var schema = Joi.object().keys({
    name: Joi.string().min(3).max(50).required().label("Name"),
    email: Joi.string().min(3).max(50).required().label("Email"),
    password: Joi.string().min(6).max(50).required().label("Password"),
    password_confirmation: Joi.string().required().valid(Joi.ref('password')).options({language: {any: {allowOnly: '!!Passwords do not match',}}}),
    hospital: Joi.string().min(3).max(50).required().label("Hospital"),
    avatar: Joi.string().empty("").label("Avatar"),
    phone: Joi.string().min(10).max(10).required().label("Phone"),
    activated: Joi.boolean().empty("").label("Activated"),
    role: Joi.string().empty("").label("Role"),
    ipAddress: Joi.string().empty("").label("Ip Address"),
    plan: Joi.string().empty("").label("Plan"),
    city: Joi.string().min(3).max(50).required().label("City"),
    state: Joi.string().min(3).max(50).required().label("State"),
    country: Joi.string().min(3).max(50).required().label("Country"),
    speciality: Joi.string().min(3).max(50).required().label("Speciality")
  });

  async function onFormSubmit(e) {
    e.preventDefault();
    console.log(registerDetails);
    const result = Joi.validate(registerDetails, schema, { abortEarly: false });
    const { error } = result;
    console.log(error)
    if (error === null) {      
      setErrors({});
      try {
        const data = await register({ registerDetails });
        console.log(data);
        if (data.status === 200) {
          setSuccessMessage(data.data.message);
          setTimeout(() => {
            // window.location.reload();
          }, 1500);
        } else {
          setSuccessMessage("Something went wrong!");
        }
      } catch (error) {
        console.log(error);
      }
    } else {
      const errorData = {};
      for (let item of error.details) {
        const name = item.path[0];
        const message = item.message;
        errorData[name] = message;
      }
      console.log(errors);
      setErrors(errorData);
      return errorData;
    }
  }

  return (
    <>
      <PageContainer>
        <Block className="nk-block-middle nk-auth-body  wide-md">
          <div className="brand-logo pb-4 text-center">
            <Link to={process.env.PUBLIC_URL + "/"} className="logo-link">
              {/* <img className="logo-light logo-img logo-img-lg" src="./assets/images/logo/hnn-logo.png" alt="logo" />               */}
              <img className="logo-img logo-img-lg" src={Logo} alt="logo" />              
            </Link>
          </div>

          <PreviewCard className="card-bordered" bodyclassName="card-inner-lg">
            <BlockHead>
              <BlockContent>
                <h4>Register</h4>                
              </BlockContent>
            </BlockHead>
            {successMessage && (
              <div className="mb-3">
                <Alert color="success" className="alert-icon">
                  {" "}
                  <Icon name="alert-circle" /> {successMessage}{" "}
                </Alert>
              </div>
            )}
            
            <Form className="is-alter">
              <Row>
                {/* <Col md="12" className="p-0"> */}
                  <Col md="6">                  
                    <Inputs label={errors.name && <>{errors.name}</>} text="Name" type="text" name="name" placeholder="Enter your name" id="name" htmlfor="name" size="md" onChange={(e) => setRegisterDetails({ ...registerDetails, name: e.target.value })} />                    
                  </Col>
                  <Col md="6">
                    <Inputs label={errors.email && <>{errors.email}</>} text="Email" type="email" name="email" placeholder="Enter your email" id="email" htmlfor="email" size="md" onChange={(e) => setRegisterDetails({ ...registerDetails, email: e.target.value })} />                    
                  </Col>                  
                  <Col md="6">                  
                    <Inputs label={errors.password && <>{errors.password}</>} text="Password" type="password" name="password" placeholder="Enter your password" id="password" htmlfor="password" size="md" onChange={(e) => setRegisterDetails({ ...registerDetails, password: e.target.value })} />                    
                  </Col>
                  <Col md="6">
                    <Inputs label={errors.password_confirmation && <>{errors.password_confirmation}</>} text="Confirm Password" type="password" name="password_confirmation" placeholder="Enter Confirm Password" id="password_confirmation" htmlfor="password_confirmation" size="md" onChange={(e) => setRegisterDetails({ ...registerDetails, password_confirmation: e.target.value })} />                    
                  </Col>
                  <Col md="6">                  
                    <Inputs label={errors.phone && <>{errors.phone}</>} text="Phone" type="text" name="phone" placeholder="Enter your Contact" id="phone" htmlfor="phone" size="md" onChange={(e) => setRegisterDetails({ ...registerDetails, phone: e.target.value })} />                    
                  </Col>
                  <Col md="6">                  
                    <Inputs label={errors.hospital && <>{errors.hospital}</>} text="Hospital Name" type="text" name="hospital" placeholder="Enter your Hospital Name" id="hospital" htmlfor="hospital" size="md" onChange={(e) => setRegisterDetails({ ...registerDetails, hospital: e.target.value })} />                    
                  </Col>
                  <Col md="6">
                    <Inputs label={errors.city && <>{errors.city}</>} text="City" type="city" name="city" placeholder="Enter your city" id="city" htmlfor="city" size="md" onChange={(e) => setRegisterDetails({ ...registerDetails, city: e.target.value })} />                    
                  </Col>
                  <Col md="6">
                    <Inputs label={errors.state && <>{errors.state}</>} text="State" type="text" name="state" placeholder="Enter your state" id="state" htmlfor="state" size="md" onChange={(e) => setRegisterDetails({ ...registerDetails, state: e.target.value })} />                    
                  </Col>
                  <Col md="6">
                    <Inputs label={errors.country && <>{errors.country}</>} text="Country" type="text" name="country" placeholder="Enter your country" id="country" htmlfor="country" size="md" onChange={(e) => setRegisterDetails({ ...registerDetails, country: e.target.value })} />                    
                  </Col>
                  <Col md="6">
                    <RSelect options={filterStatus} text="Speciality" placeholder="Select Speciality" name="speciality" label={errors.speciality && <>{errors.speciality}</>} onChange={(e) => setRegisterDetails({ ...registerDetails, speciality: e.target.value })} id="speciality" htmlfor="speciality" size="md" />
                </Col>
                  
              </Row>
              
              <FormGroup>
                <Button size="lg" className="btn-block" type="submit" color="primary" onClick={onFormSubmit}>
                  {loading ? <Spinner size="sm" color="light" /> : "Sign in"}
                </Button>
                <p className="mt-3 text-right">
                  Already have an account? <Link className="link link-primary link-sm font_16" to={`${process.env.PUBLIC_URL}/login`}>
                  Log In
                </Link>
                </p>
              </FormGroup>
            </Form>            
          </PreviewCard>
        </Block>
        <AdminFooter />
      </PageContainer>
    </>
  );
};
export default AdminRegister;

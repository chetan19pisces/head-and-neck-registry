const menu = [
  {
    heading: "Dashboard",
  },
  {
    icon: "dashboard-fill",
    text: "Dashboard",
    link: "/dashboard",
  },
  {
    icon: "plus-medi-fill",
    text: "Add Patient",
    active: false,
    subMenu: [
      {
        icon: "users",
        text: "Demography",
        link: "/demography",
      },
      {
        icon: "sign-ada",
        text: "Initial Presentation",
        link: "/initial-presentation",
      },      
    ],
  },
];
export default menu;

import React, { useEffect, useState } from "react";
import { Button, Card, Col, Row, Spinner } from "reactstrap";
import axios from "axios";
import Icon from "../../adminComponents/icon/Icon";
import styles from "./HeaderSearch.module.css";
import { Block, BlockBetween, BlockHead, BlockHeadContent, BlockTitle } from "../../adminComponents/Component";
import { Link } from "react-router-dom";
import Content from "./Content";

const SingleDetail = (props) => {
  const [users, setUsers] = useState([]);
  const [loading, setLoading] = useState(false);
  const userId = props.userId;

  const getUserDetails = async () => {
    setLoading(true);
    await axios(
      `${process.env.REACT_APP_DEFAULT_URL}/api/v1/admin/analytics/user-details/${userId}`
    )
      .then((response) => {
        console.log(response.data.analyticSingleUserId);
        setUsers([response.data.analyticSingleUserId]);
      })
      .catch((error) => {
        console.log(error.message);
      });
    setLoading(false);
  };

  useEffect(() => {
    userId && getUserDetails();
  }, [userId]);

  return (
    <>
      <Content>
        <Card className="card-full" style={{ background: "transparent" }}>
          {loading && (
            <div className="text-center">
              <Spinner color="dark" />
            </div>
          )}
          <Block>
            <div className="is-separate is-medium mb-3">
              {userId &&
                users.map((user) => (
                  <>
                    <div className="border-bottom mb-2 pb-2" key={userId}>
                    <BlockHead size="sm">
                      <BlockBetween className="g-3">
                        <BlockHeadContent>
                          <BlockTitle page>
                            <strong className="text-primary">{user.name}</strong>
                          </BlockTitle>         
                        </BlockHeadContent>
                        <BlockHeadContent>
                          <Link to={`${process.env.PUBLIC_URL}/user-tracking/${userId}`} className="mr-2">
                            <Button color="primary" outline className="primary d-none d-sm-inline-flex">
                              <Icon name="growth"></Icon>
                              <span>Analytics</span>
                            </Button>
                            <Button color="primary" outline className="primary btn-icon d-inline-flex d-sm-none">
                              <Icon name="growth"></Icon>
                            </Button>
                          </Link>
                          <Link to={`${process.env.PUBLIC_URL}/all-members`}>
                            <Button color="dark" outline className="primary d-none d-sm-inline-flex">
                              <Icon name="arrow-left"></Icon>
                              <span>Back</span>
                            </Button>
                            <Button color="dark" outline className="primary btn-icon d-inline-flex d-sm-none">
                              <Icon name="arrow-left"></Icon>
                            </Button>
                          </Link>
                          </BlockHeadContent>
                      </BlockBetween>
                    </BlockHead>
                    </div>
                    <table className="table table-striped table-dark table-bordered">
                      <tbody>
                        <tr>
                          <td className={styles.font_16}>Email</td>
                          <td className={styles.font_16}>{user.email ? user.email : "-"}</td>
                        </tr>
                        <tr>
                          <td className={styles.font_16}>Phone</td>
                          <td className={styles.font_16}>{user.phone ? user.phone : "-"}</td>
                        </tr>
                        <tr>
                          <td className={styles.font_16}>Country</td>
                          <td className={styles.font_16}>{user.country ? user.country : "-"}</td>
                        </tr>
                        <tr>
                          <td className={styles.font_16}>State</td>
                          <td className={styles.font_16}>{user.state ? user.state : "-"}</td>
                        </tr>
                        <tr>
                          <td className={styles.font_16}>City</td>
                          <td className={styles.font_16}>{user.city ? user.city : "-"}</td>
                        </tr>
                        <tr>
                          <td className={styles.font_16}>Designation</td>
                          <td className={styles.font_16}>{user.designation ? user.designation : "-"}</td>
                        </tr>
                        <tr>
                          <td className={styles.font_16}>Speciality</td>
                          <td className={styles.font_16}>{user.speciality ? user.speciality : "-"}</td>
                        </tr>
                        <tr>
                          <td className={styles.font_16}>Hospital</td>
                          <td className={styles.font_16}>{user.hosptial ? user.hosptial : "-"}</td>
                        </tr>
                        <tr>
                          <td className={styles.font_16}>Clinic</td>
                          <td className={styles.font_16}>{user.clinic ? user.clinic : "-"}</td>
                        </tr>
                        <tr>
                          <td className={styles.font_16}>Role</td>
                          <td style={{fontWeight: "bold"}}>{user.role ? user.role.toUpperCase() : "-"}</td>
                        </tr>
                      </tbody>
                    </table>
                  </>
                ))}
            </div>
          </Block>
        </Card>
      </Content>
    </>
  );
};

export default SingleDetail;

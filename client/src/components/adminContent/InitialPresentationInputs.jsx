import React, { useEffect, useState } from "react";
import {Block, BlockContent, BlockHead, Button, Icon, PreviewCard } from "../adminComponents/Component";
import { Form, Spinner, Alert, Row, Col } from "reactstrap";
import PageContainer from "../adminLayout/Layouts/PageContainer";
import Joi from "joi-browser";
import { initialpresentations } from '../../http/index'
import Inputs from "../adminComponents/input/Inputs";
import Radios from "../adminComponents/input/Radios";
import { MultiSelect, RSelect } from "../adminComponents/select/ReactSelect";
import { at_diagnosis, cM, cM_Based, cN, cN_Based, cT, cT_Based, first_treatment_given, genetics, germline_testing, laterality, metastases, presentation, total_no_of_metastases, types_of_metastases } from '../adminComponents/constants/InitialPresentationConstants'
import { useNavigate, useParams } from "react-router-dom";

const InitialPresentationInputs = () => {  
  const { code } = useParams();
  const [loading, setLoading] = useState(false);  

  const navigate = useNavigate();
  const [initialPresentation, setInitialPresentation] = useState({  
    presentation: "", at_diagnosis: "", laterality: "", cT: "", cN: "", cM: "", cT_Based: "", cN_Based: "", cM_Based: "", metastases: "", total_no_of_metastases: "", types_of_metastases: "", other_metastases_type: "", first_treatment_given: "", germline_testing: "", genetics: "", other_genetics: "", code: code
  });

  const [errors, setErrors] = useState({});
  const [successMessage, setSuccessMessage] = useState("");

  var schema = Joi.object().keys({    
    presentation: Joi.string().required().label("Presentation"),
    at_diagnosis: Joi.string().required().label("At Diagnosis"),
    laterality: Joi.string().required().label("Laterality"),
    metastases: Joi.string().required().label("Metastases"),
    cT: Joi.string().required().label("cT"),
    cN: Joi.string().required().label("cN"),
    cM: Joi.string().required().label("cM"),
    cT_Based: Joi.array().required().label("cT Based on Criteria"),
    cN_Based: Joi.array().required().label("cN Based on Criteria"),
    cM_Based: Joi.array().required().label("cM Based on Criteria"),
    total_no_of_metastases: Joi.when('metastases', {
      is: Joi.exist().valid('Yes'),
      then: Joi.string().required().label("Total Number of Metastases"),
    }),
    types_of_metastases: Joi.when('metastases', {
      is: Joi.exist().valid('Yes'),
      then: Joi.array().required().label("Types of Metastases"),
    }),
    other_metastases_type: Joi.when('types_of_metastases', {
      is: Joi.array().valid('Other'),
      then: Joi.array().required().label("Other Type of Metastases"),
    }),
    first_treatment_given: Joi.array().required().label("First Treatment Given"),
    germline_testing: Joi.string().required().label("Germline Testing"),
    genetics: Joi.when('germline_testing', {
      is: Joi.exist().valid('Yes'),
      then: Joi.string().required().label("Genetics"),
    }),
    other_genetics: Joi.when('genetics', {
      is: Joi.exist().valid('Other'),
      then: Joi.string().required().label("Other Genetics"),
    }),
    code: Joi.empty().label("Code"),
  });

  async function onFormSubmit(e) {
    setLoading(true)
    e.preventDefault();    
    let finalData = [];  
    finalData = {
      ...initialPresentation,
    };
    console.log(finalData);  
    const result = Joi.validate(initialPresentation, schema, { abortEarly: false });
    const { error } = result;    
    if (error === null) {      
      setErrors({});
      try {
        finalData = {
          ...initialPresentation,
        };       
        const data = await initialpresentations(code, { finalData });        
        if (data.status === 200) {
          setSuccessMessage(data.data.message);
          setTimeout(() => {
            navigate(`/dashboard`, { replace: true });
          }, 1500);
          setLoading(false);
        } else {
          setSuccessMessage("Something went wrong!");
        }
      } catch (error) {
        console.log(error);
      }
    } else {      
      finalData = {
        ...initialPresentation,
      };
      const errorData = {};
      for (let item of error.details) {
        const name = item.path[0];
        const message = item.message;
        errorData[name] = message;
      }
      console.log(errors);
      setErrors(errorData);
      setTimeout(() => {
        setLoading(false)      
      }, 1000)
      
      return errorData;
    }    
  }

  useEffect(() => {
  }, []);

  return (
    <>    
      <PageContainer>
        <Block className="container-fluid p-0">
          <PreviewCard className="card-bordered" bodyclassName="card-inner-lg">
            <BlockHead>
              <BlockContent>
                <h4>Patient's Initial Presentation</h4>                
              </BlockContent>
            </BlockHead>
            <Form className="is-alter">
              <Row>
                {/* <Col md="12" className="p-0"> */}

                <Col md="3">
                  <RSelect options={presentation} text="Presentation" placeholder="Select Presentation" name="presentation" label={errors.presentation && <>{errors.presentation}</>} onChange={(e) => setInitialPresentation({ ...initialPresentation, presentation: e.target.value })} id="presentation" htmlfor="presentation" size="md" />
                </Col>
                <Col md="3">
                  <RSelect options={at_diagnosis} text="At Diagnosis" placeholder="Select At Diagnosis" name="at_diagnosis" label={errors.at_diagnosis && <>{errors.at_diagnosis}</>} onChange={(e) => setInitialPresentation({ ...initialPresentation, at_diagnosis: e.target.value })} id="at_diagnosis" htmlfor="at_diagnosis" size="md" />
                </Col>
                <Col md="3">
                  <RSelect options={laterality} text="Laterality" placeholder="Select Laterality" name="laterality" label={errors.laterality && <>{errors.laterality}</>} onChange={(e) => setInitialPresentation({ ...initialPresentation, laterality: e.target.value })} id="laterality" htmlfor="laterality" size="md" />
                </Col>

                <Col md="12"><hr className="hr" /></Col>

                <h5 className="px-2 pt-2">Original Clinical Stage</h5>  

                <Col md="12"><hr className="hr" /></Col>

                <Col md="1">
                  <RSelect options={cT} text="cT" placeholder="Select cT" name="cT" label={errors.cT && <>{errors.cT}</>} onChange={(e) => setInitialPresentation({ ...initialPresentation, cT: e.target.value })} id="cT" htmlfor="cT" size="md" />
                </Col>
                <Col md="3">
                  <MultiSelect options={cT_Based} text="Based On cT Criteria" placeholder="Select cT_Based" name="cT_Based" label={errors.cT_Based && <>{errors.cT_Based}</>} onChange={(e) => setInitialPresentation({ ...initialPresentation, cT_Based: Array.isArray(e) ? e.map(x => x.value) : [] })} id="cT_Based" htmlfor="cT_Based" size="md" />
                </Col>
                <Col md="1">
                  <RSelect options={cN} text="cN" placeholder="Select cN" name="cN" label={errors.cN && <>{errors.cN}</>} onChange={(e) => setInitialPresentation({ ...initialPresentation, cN: e.target.value })} id="cN" htmlfor="cN" size="md" />
                </Col>
                <Col md="3">
                  <MultiSelect options={cN_Based} text="Based On cN Criteria" placeholder="Select cN_Based" name="cN_Based" label={errors.cN_Based && <>{errors.cN_Based}</>} onChange={(e) => setInitialPresentation({ ...initialPresentation, cN_Based: Array.isArray(e) ? e.map(x => x.value) : [] })} id="cN_Based" htmlfor="cN_Based" size="md" />
                </Col>
                <Col md="1">
                  <RSelect options={cM} text="cM" placeholder="Select cM" name="cM" label={errors.cM && <>{errors.cM}</>} onChange={(e) => setInitialPresentation({ ...initialPresentation, cM: e.target.value })} id="cM" htmlfor="cM" size="md" />
                </Col>
                <Col md="3">
                  <MultiSelect options={cM_Based} text="Based On cM Criteria" placeholder="Select cM Based" name="cM_Based" label={errors.cM_Based && <>{errors.cM_Based}</>} onChange={(e) => setInitialPresentation({ ...initialPresentation, cM_Based: Array.isArray(e) ? e.map(x => x.value) : [] })} id="cM_Based" htmlfor="cM_Based" size="md" />
                </Col>

                <Col md="12"><hr className="hr" /></Col>

                <Col md="2" style={{zIndex: "0"}}>
                  <Radios options={metastases} label={errors.metastases && <>{errors.metastases}</>} text="Metastases?" type="radio" name="metastases" id="metastases" size="md" onChange={(e) => setInitialPresentation({ ...initialPresentation, metastases: e.target.value })} />
                </Col>
                {initialPresentation.metastases === "Yes" && (
                  <>
                    <Col md="3" style={{zIndex: "0"}}>
                      <Radios options={total_no_of_metastases} label={errors.total_no_of_metastases && <>{errors.total_no_of_metastases}</>} text="Total number of metastatic (Lesions)" type="radio" name="total_no_of_metastases" id="total_no_of_metastases" size="md" onChange={(e) => setInitialPresentation({ ...initialPresentation, total_no_of_metastases: e.target.value })} />
                    </Col>
                    <Col md="3">
                      <MultiSelect options={types_of_metastases} text="Types of Metastases" placeholder="Types of Metastases" name="types_of_metastases" label={errors.types_of_metastases && <>{errors.types_of_metastases}</>} onChange={(e) => setInitialPresentation({ ...initialPresentation, types_of_metastases: Array.isArray(e) ? e.map(x => x.value) : [] })} id="types_of_metastases" htmlfor="types_of_metastases" size="md" />
                    </Col>
                    {
                      initialPresentation.types_of_metastases.includes("Other") && initialPresentation.types_of_metastases.length > 0 && (
                        <Col md="3">                  
                          <Inputs label={errors.other_metastases_type && <>{errors.other_metastases_type}</>} text="Other Metastases Type" type="text" name="other_metastases_type" placeholder="Enter Other Metastases Type" id="other_metastases_type" htmlfor="other_metastases_type" size="md" onChange={(e) => setInitialPresentation({ ...initialPresentation, other_metastases_type: e.target.value })} />
                        </Col>
                      )
                    }
                  </>
                )}

                <Col md="12"><hr className="hr" /></Col>
                
                <Col md="3">
                  <MultiSelect options={first_treatment_given} text="First Treatment Given" placeholder="First Treatment Given" name="first_treatment_given" label={errors.first_treatment_given && <>{errors.first_treatment_given}</>} onChange={(e) => setInitialPresentation({ ...initialPresentation, first_treatment_given: Array.isArray(e) ? e.map(x => x.value) : [] })} id="first_treatment_given" htmlfor="first_treatment_given" size="md" />
                </Col>
                
                <Col md="3" style={{zIndex: "0"}}>
                  <Radios options={germline_testing} label={errors.germline_testing && <>{errors.germline_testing}</>} text="Germline testing done" type="radio" name="germline_testing" id="germline_testing" size="md" onChange={(e) => setInitialPresentation({ ...initialPresentation, germline_testing: e.target.value })} />
                </Col>
                
                {initialPresentation.germline_testing === "Yes" && (                    
                  <Col md="3">                  
                    <RSelect options={genetics} text="Genetics" placeholder="Select Genetics" name="genetics" label={errors.genetics && <>{errors.genetics}</>} onChange={(e) => setInitialPresentation({ ...initialPresentation, genetics: e.target.value })} id="genetics" htmlfor="genetics" size="md" />                   
                  </Col>
                )}
                {
                  initialPresentation.genetics === "Other" && (
                    <Col md="3">                  
                      <Inputs label={errors.other_genetics && <>{errors.other_genetics}</>} text="Other Genetics" type="text" name="other_genetics" placeholder="Enter Other Genetics" id="other_genetics" htmlfor="other_genetics" size="md" onChange={(e) => setInitialPresentation({ ...initialPresentation, other_genetics: e.target.value })} />                    
                    </Col>
                  )
                }                  
              </Row>
              
              <Col md="3" className="p-0 pull-right">
                <Button type="submit" color="primary" onClick={onFormSubmit}>
                  Next {loading ? <Spinner size="sm" color="light" /> : ""}
                </Button>                          
              </Col>
            </Form>  

            {successMessage && (
              <div className="mb-3 mt-3">
                <Alert color="success" className="alert-icon">
                  {" "}
                  <Icon name="alert-circle" /> {successMessage}{" "}
                </Alert>
              </div>
            )}

          </PreviewCard>
        </Block>        
      </PageContainer>
    </>
  );
};
export default InitialPresentationInputs;

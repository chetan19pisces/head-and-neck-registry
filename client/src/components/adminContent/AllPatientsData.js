import React, { useEffect, useState } from "react";
import { Button, Card, Spinner } from "reactstrap";
import { DataTableHead, DataTableRow, DataTableItem, UserAvatar, Block, PreviewAltCard, PaginationComponent, Icon } from "../adminComponents/Component";
import moment from 'moment';
import axios from "axios";
import { Link } from "react-router-dom";
// import { recentOrderData } from "./OrderData";

const AllPatientsData = () => {
  const [allpatientsData, setAllPatientsData] = useState([]);
  const [loading, setLoading] = useState(false);
  const [currentPage, setCurrentPage] = useState(1);  
  const [itemPerPage] = useState(10);  


  async function getAllPatientsData() {
    setLoading(true);
    await fetch(
      `${process.env.REACT_APP_DEFAULT_URL}/api/v1/admin/account/all`,
      {
        method: "GET",
      }
    )
      .then((response) => response.json())
      .then((res) => {
        setAllPatientsData(res.allUsers);
        setLoading(false);
      }); 
  }

  async function updateUserStatus(e, status){       
    setLoading(true);    
      await axios.post(`${process.env.REACT_APP_DEFAULT_URL}/api/v1/admin/users/${status}/${e}`).then(res => {
      setLoading(false); 
      getAllPatientsData();
    })
  }

  const indexOfLastItem = currentPage * itemPerPage;
  const indexOfFirstItem = indexOfLastItem - itemPerPage;
  const currentItems = allpatientsData.slice(indexOfFirstItem, indexOfLastItem);
  const paginate = (pageNumber) => setCurrentPage(pageNumber);

  useEffect(() => {
    getAllPatientsData();
  }, [])

  return (
    <Card className="card-full" style={{ "background": "transparent"}}>
      {/* <div className="card-inner" style={{ "background": "#fff"}}>
        <div className="card-title-group">
          <div className="card-title">
            <h6 className="titles">Survey Data</h6>
          </div>
        </div>
      </div> */}
      {loading && <div className="text-center"><Spinner color="dark" /></div>}
      <Block>
          <div className="nk-tb-list is-separate is-medium mb-3">      
        <DataTableHead>
          <DataTableRow>
            <span>ID.</span>
          </DataTableRow>
          <DataTableRow>
            <span>Name</span>
          </DataTableRow>
          <DataTableRow size="sm">
            <span>Email</span>
          </DataTableRow>
          {/* <DataTableRow size="md">
            <span>Avatar</span>
          </DataTableRow> */}
          <DataTableRow>
            <span>Account Created</span>
          </DataTableRow>
          <DataTableRow>
            <span className="d-none d-sm-inline">Status</span>
          </DataTableRow>
        </DataTableHead>
        {/* {AllpatientsData.map((item, idx) => ( */}
        {currentItems.length > 0
          ? currentItems.map((item, idx) => (
          <DataTableItem key={idx}>
            <DataTableRow>
              <span className="tb-lead">
                <a href="#order" onClick={(ev) => ev.preventDefault()}>
                  {idx + 1}
                </a>
              </span>
            </DataTableRow>
            <DataTableRow size="sm">
              <div className="user-card">
                <UserAvatar className="sm" theme="purple-dim" text={item.country} image={item.avatar}></UserAvatar>
                <div className="user-name">
                  <span className="tb-lead">{item.name}</span>
                </div>
              </div>
            </DataTableRow>
            <DataTableRow size="md">
              <span className="tb-sub">{item.email}</span>
            </DataTableRow>
            <DataTableRow>
              <span className="tb-sub tb-amount">
                {moment(item.createdAt).format('MMMM Do YYYY, h:mm:ss a')}
              </span>
            </DataTableRow>
            <DataTableRow>
              {item.activated === true ? (
                <div className="w-100 text-center d-block">
                  <Link to={'/admin/user-details/' + item._id} className="mr-2">
                    <Button color="secondary">
                      <Icon name="eye" /> <span>Details</span>
                    </Button>
                  </Link>
                  <Button color="primary" onClick={() => updateUserStatus(item._id, false)}>
                    <Icon name="cross" /> <span>Deactivate</span>
                  </Button>
                </div>
                ) : (
                <div className="w-100 text-center d-block">
                  <Link to={'/admin/user-details/' + item._id} className="mr-2">
                    <Button color="secondary">
                      <Icon name="eye" /> <span>Details</span>
                    </Button>
                  </Link>
                  <Button color="danger" onClick={() => updateUserStatus(item._id, true)}>
                    <Icon name="lock" /> <span>Activate</span>
                  </Button>
                </div>
                )}
            </DataTableRow>
          </DataTableItem>
        )) : (
          null
        )
      }
      </div>
      <PreviewAltCard>
        {currentItems.length > 0 ? (
          <PaginationComponent
            itemPerPage={itemPerPage}
            totalItems={allpatientsData.length}
            paginate={paginate}
            currentPage={currentPage}
          />
        ) : (
          <div className="text-center">
            <span className="text-silent">No data found</span>
          </div>
        )}
      </PreviewAltCard>
      </Block>
    </Card>
  );
};
export default AllPatientsData;

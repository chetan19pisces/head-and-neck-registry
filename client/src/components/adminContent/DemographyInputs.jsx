import React, { useEffect, useState } from "react";
import {Block, BlockContent, BlockHead, Button, Icon, PreviewCard } from "../adminComponents/Component";
import { Form, Spinner, Alert, Row, Col } from "reactstrap";
import PageContainer from "../adminLayout/Layouts/PageContainer";
import Joi from "joi-browser";
import { demographys } from "../../http/index";
import Inputs from "../adminComponents/input/Inputs";
import Radios from "../adminComponents/input/Radios";
import DatePickerInput from "../adminComponents/input/DatePickerInput";
import { MultiSelect, RSelect } from "../adminComponents/select/ReactSelect";
import { co_morbodities, diets, educational_qualification, hasCancer, intake_types, patient_ethnicity, patient_relative, praffinBlocks, presenting_symptoms, type_of_cancers, yes_no } from '../adminComponents/constants/DemographyConstants'
import { useSelector } from 'react-redux';
import { useNavigate } from "react-router-dom";


const DemographyInputs = () => {
  const { user } = useSelector((state) => state.authenticationSlice);
  const [loading, setLoading] = useState(false);  
  const [errorVal, setError] = useState("");
  const navigate = useNavigate();
  const [demography, setDemography] = useState({  
    patient_name: "", city: "", country: "", hospital_id: user.name, patient_initial: "", date_of_birth: "", age_at_diagnosis: "", date_of_diagnosis: "", praffin_blocks: "", educational_qualification: "", other_qualification: "", ethnicity: "", other_ethnicity: "", height: "", weight: "", bmi: 0, bsa: 0, has_cancer: "", which_relative: "", presenting_symptom: "", co_morboditie: "", other_co_morboditie: "", tobacco: "", intake_type: "", alcohol: "", number_of_pegs: "", number_of_years_alcohol_addiction: "", diet: "", presenting_symptoms: "", code: (Math.random() + Math.random() + 1).toString(24).substring(2),
  });
  const [relativeCancer, setRelativeCancer] = useState([]);
  const [intakeType, setIntakeType] = useState([]);
  const [errors, setErrors] = useState({});
  const [successMessage, setSuccessMessage] = useState("");

  var schema = Joi.object().keys({    
    age_at_diagnosis: Joi.string().required().label("Age at Diagnosis"),
    alcohol: Joi.string().required().label("Alcohol"),
    bmi: Joi.number().required().label("BMI"),
    bsa: Joi.number().required().label("BSA"),
    city: Joi.string().min(3).max(50).required().label("City"),
    co_morboditie: Joi.string().required().label("Co Morbodities"),
    country: Joi.string().min(3).max(50).required().label("Country"),
    date_of_birth: Joi.any().required().label("Date of Birth"),
    date_of_diagnosis: Joi.any().required().label("Date of Diagnosis"),
    diet: Joi.string().required().label("Diet"),
    educational_qualification: Joi.string().required().label("Educational Qualification"),
    ethnicity: Joi.string().required().label("Ethnicity"),    
    has_cancer: Joi.string().required().label("Has Cancer"),
    height: Joi.string().min(3).max(50).required().label("Height"),
    hospital_id: Joi.empty().label("Hospital ID"),
    // number_of_pegs: Joi.string().required().label("Number of Pegs"),
    number_of_pegs: Joi.when('alcohol', {
      is: Joi.exist().valid('Yes'),
      then: Joi.string().required().label("Number of Pegs"),
    }),
    other_ethnicity: Joi.when('ethnicity', {
      is: Joi.exist().valid('Other'),
      then: Joi.string().required().label("Other Ethnicity"),
    }),
    other_co_morboditie: Joi.when('co_morboditie', {
      is: Joi.exist().valid('Other'),
      then: Joi.string().required().label("Other Co Morboditie"),
    }),
    other_qualification: Joi.when('educational_qualification', {
      is: Joi.exist().valid('Other'),
      then: Joi.string().required().label("Other Qualification"),
    }),
    patient_initial: Joi.string().required().label("Patient Initial"),
    patient_name: Joi.string().min(3).max(50).required().label("Patient Name"),
    praffin_blocks: Joi.string().required().label("Praffin Blocks"),
    intake_type: Joi.when('tobacco', {
      is: Joi.exist().valid('Yes'),
      then: Joi.array().required().label("Tobacco Intake Type"),
    }),
    tobacco: Joi.string().required().label("Tobacco"),    
    weight: Joi.string().required().label("Weight"),
    which_relative: Joi.when('has_cancer', {
      is: Joi.exist().valid('Yes'),
      then: Joi.array().required().label("Relative"),
    }),    
    number_of_years_alcohol_addiction: Joi.when('alcohol', {
      is: Joi.exist().valid('Yes'),
      then: Joi.string().required().label("Number of Years Alcohol Addiction"),
    }),
    presenting_symptom: Joi.empty().label("Presenting Symptoms"),
    presenting_symptoms: Joi.empty().label("Presenting Symptoms"),
    code: Joi.empty().label("Code"),
  });

  async function onFormSubmit(e) {
    setLoading(true)    
    e.preventDefault();    
    let finalData = [];  
    finalData = {
      ...demography,
      ...relativeCancer,
      ...intakeType
    };
    console.log(finalData);
    const result = Joi.validate(demography, schema, { abortEarly: false });
    const { error } = result;
    if (error === null) {
      setErrors({});
      try {
        finalData = {
          ...demography,
          ...relativeCancer,
          ...intakeType
        };
        const data = await demographys({ finalData });        
        if (data.status === 201) {
          setSuccessMessage(data.data.message);
          setTimeout(() => {
            navigate(`/initial-presentation/${demography.code}`, { replace: true });
          }, 1000);
          setLoading(false);
        } else {
          setSuccessMessage("Something went wrong!");
        }
      } catch (error) {
        console.log(error);
      }
    } else {      
      finalData = {
        ...demography,
        ...relativeCancer,
        ...intakeType
      };
      const errorData = {};
      for (let item of error.details) {
        const name = item.path[0];
        const message = item.message;
        errorData[name] = message;
      }
      console.log(errors);
      setErrors(errorData);
      setTimeout(() => {
        setLoading(false)      
      }, 1000)
      
      return errorData;
    }    
  }

  useEffect(() => {    

    

  }, []);

  const calculate_BMI_BSA_Height = (e) => {
    const bmi_value = parseFloat((demography.weight / Math.pow(e.target.value, 2))*10000).toFixed(2);
    const bsa_value = parseFloat(Math.sqrt((demography.weight * e.target.value) / 3600)).toFixed(2);
    setDemography({
      ...demography,
      [e.target.name]: e.target.value,
      bmi: bmi_value,
      bsa: bsa_value,
    })        
  }
  
  const calculate_BMI_BSA_Weight = (e) => {
    const bmi_value = parseFloat((e.target.value / Math.pow(demography.height, 2))*10000).toFixed(2);
    const bsa_value = parseFloat(Math.sqrt((e.target.value * demography.height) / 3600)).toFixed(2);
    setDemography({
      ...demography,
      [e.target.name]: e.target.value,
      bmi: bmi_value,
      bsa: bsa_value,
    })        
  }

  return (
    <>    
      <PageContainer>
        <Block className="container-fluid p-0">
          <PreviewCard className="card-bordered" bodyclassName="card-inner-lg">
            <BlockHead>
              <BlockContent>
                <h4>Demography</h4>                
              </BlockContent>
            </BlockHead>
            <Form className="is-alter">
              <Row>
                {/* <Col md="12" className="p-0"> */}
                  <Col md="3">                  
                    <Inputs label={errors.patient_name && <>{errors.patient_name}</>} text="Patient Name" type="text" name="patient_name" placeholder="Enter Patient Name" id="patient_name" htmlfor="patient_name" size="md" onChange={(e) => setDemography({ ...demography, patient_name: e.target.value })} />                    
                  </Col>
                  <Col md="3">
                    <Inputs label={errors.city && <>{errors.city}</>} text="City" type="city" name="city" placeholder="Enter Patient City" id="city" htmlfor="city" size="md" onChange={(e) => setDemography({ ...demography, city: e.target.value })} />                    
                  </Col>
                  <Col md="3">
                    <Inputs label={errors.country && <>{errors.country}</>} text="Country" type="text" name="country" placeholder="Enter Patient Country" id="country" htmlfor="country" size="md" onChange={(e) => setDemography({ ...demography, country: e.target.value })} />                    
                  </Col>                  
                  <Col md="3">
                    <Inputs label={errors.hospital_id && <>{errors.hospital_id}</>} text="Hospital ID" type="text" name="hospital_id" placeholder="Enter Patient Hospital ID" id="hospital_id" htmlfor="hospital_id" size="md" onChange={(e) => setDemography({ ...demography, hospital_id: e.target.value })} value={user.name} disable={true} />                    
                  </Col>                  
                  <Col md="3">
                    <Inputs label={errors.patient_initial && <>{errors.patient_initial}</>} text="Patient's Initial" type="text" name="patient_initial" placeholder="Enter Patient's Initial" id="patient_initial" htmlfor="patient_initial" size="md" onChange={(e) => setDemography({ ...demography, patient_initial: e.target.value })} />                    
                  </Col>                  
                                    
                  <Col md="3">
                    <DatePickerInput label={errors.date_of_birth && <>{errors.date_of_birth}</>} text="Date of Birth" type="text" name="date_of_birth" placeholder="Select Date of Birth" id="date_of_birth" htmlfor="date_of_birth" size="md" selected={demography.date_of_birth} startDate={demography.date_of_birth} onChange={(e) => setDemography({ ...demography, date_of_birth: e })} />
                  </Col>                  
                  <Col md="3">
                    <Inputs label={errors.age_at_diagnosis && <>{errors.age_at_diagnosis}</>} text="Age at Diagnosis" type="number" name="age_at_diagnosis" placeholder="Age at Diagnosis" id="age_at_diagnosis" htmlfor="age_at_diagnosis" size="md" onChange={(e) => setDemography({ ...demography, age_at_diagnosis: e.target.value })} />                    
                  </Col>
                  <Col md="3">
                    <DatePickerInput label={errors.date_of_diagnosis && <>{errors.date_of_diagnosis}</>} text="Date of Diagnosis" type="text" name="date_of_diagnosis" placeholder="Select Date of Diagnosis" id="date_of_diagnosis" htmlfor="date_of_diagnosis" size="md" selected={demography.date_of_diagnosis} startDate={demography.date_of_diagnosis} onChange={(e) => setDemography({ ...demography, date_of_diagnosis: e })} />           
                  </Col>
                  <Col md="12"><hr className="hr" /></Col>
                  <Col md="3">
                    <Radios options={praffinBlocks} label={errors.praffin_blocks && <>{errors.praffin_blocks}</>} text="Paraffin blocks available?" type="radio" name="praffin_blocks" id="praffin_blocks" size="md" onChange={(e) => setDemography({ ...demography, praffin_blocks: e.target.value })} />
                  </Col>
                  <Col md="3">
                    <RSelect options={educational_qualification} text="Educational Level" placeholder="Select Educational Level" name="educational_qualification" label={errors.educational_qualification && <>{errors.educational_qualification}</>} onChange={(e) => setDemography({ ...demography, educational_qualification: e.target.value })} id="educational_qualification" htmlfor="educational_qualification" size="md" />
                  </Col>
                  {demography.educational_qualification === "Other" && (
                    <Col md="2">
                    <Inputs label={errors.other_qualification && <>{errors.other_qualification}</>} text="Other Qualification" type="text" name="other_qualification" placeholder="Other Qualification" id="other_qualification" htmlfor="other_qualification" size="md" onChange={(e) => setDemography({ ...demography, other_qualification: e.target.value })} />                    
                  </Col>
                  )}
                  <Col md="2">
                    <RSelect options={patient_ethnicity} text="Ethnicity" placeholder="Select Ethnicity" name="ethnicity" label={errors.ethnicity && <>{errors.ethnicity}</>} onChange={(e) => setDemography({ ...demography, ethnicity: e.target.value })} id="ethnicity" htmlfor="ethnicity" size="md" />
                  </Col>
                  {demography.ethnicity === "Other" && (
                    <Col md="2">
                    <Inputs label={errors.other_ethnicity && <>{errors.other_ethnicity}</>} text="Other Ethnicity" type="text" name="other_ethnicity" placeholder="Other Ethnicity" id="other_ethnicity" htmlfor="other_ethnicity" size="md" onChange={(e) => setDemography({ ...demography, other_ethnicity: e.target.value })} />                    
                  </Col>
                  )}
                  <Col md="12"><hr className="hr" /></Col>
                  <Col md="3">                  
                    <Inputs label={errors.height && <>{errors.height}</>} text="Enter Height" type="number" name="height" placeholder="Enter Height" id="height" htmlfor="height" size="md" onChange={(e) => calculate_BMI_BSA_Height(e)} />
                  </Col>
                  <Col md="3">
                    <Inputs label={errors.weight && <>{errors.weight}</>} text="Enter Weight" type="number" name="weight" placeholder="Enter Weight" id="weight" htmlfor="weight" size="md" onChange={(e) => calculate_BMI_BSA_Weight(e)} />
                  </Col>
                  <Col md="3">
                    <Inputs label={errors.bmi && <>{errors.bmi}</>} text="BMI Value" type="text" name="bmi" placeholder="BMI Value" id="bmi" htmlfor="bmi" size="md" onChange={(e) => setDemography({ ...demography, bmi: e.target.value })} disable={true} value={demography.bmi} />                    
                  </Col>                  
                  <Col md="3">
                  <Inputs label={errors.bsa && <>{errors.bsa}</>} text="BSA Value" type="text" name="bsa" placeholder="BSA Value" id="bsa" htmlfor="bsa" size="md" onChange={(e) => setDemography({ ...demography, bsa: e.target.value })} disable={true} value={demography.bsa} />
                  </Col>
                  <Col md="12"><hr className="hr" /></Col>
                  <Col md="2">
                    <Radios options={hasCancer} label={errors.has_cancer && <>{errors.has_cancer}</>} text="Family Member has Cancer?" type="radio" name="has_cancer" id="has_cancer" size="md" onChange={(e) => setDemography({ ...demography, has_cancer: e.target.value })} value={demography.has_cancer} />
                  </Col>
                  {demography.has_cancer === "Yes" && (
                  <Col md="2">
                    <MultiSelect options={patient_relative} text="Which Relative" placeholder="Select Relative" name="which_relative" label={errors.which_relative && <>{errors.which_relative}</>} onChange={(e) => {setDemography({ ...demography, which_relative: Array.isArray(e) ? e.map(x => x.value) : [] }); }} id="which_relative" htmlfor="which_relative" size="md" />                    
                  </Col>
                  )}
                  {demography.has_cancer === "Yes" && (
                  <Col md="8">                    
                    {
                      demography.which_relative.length > 0 && (
                        demography.which_relative.map((relative, index) => (
                          <>                          
                            <Row key={index}>
                              <Col md="3">
                                <RSelect options={type_of_cancers} text={"Type of Cancer (" + relative + ")"} placeholder="Select Type of Cancer" name={"type_of_cancer_" + relative} label={errors.type_of_cancer && <>{errors.type_of_cancer}</>} onChange={(e) => setRelativeCancer({ ...relativeCancer, [e.target.name]: e.target.value })} id={"type_of_cancer_" + relative} htmlfor={"type_of_cancer_" + relative} size="md" />
                              </Col>
                              <Col md="3">
                                <Inputs label={errors.relative_age && <>{errors.relative_age}</>} text={relative + " Age at Diagnosis"} type="number" name={"relative_age_" + relative} placeholder={relative + " Age at Diagnosis"} id={"relative_age_" + relative} htmlfor={"relative_age_" + relative} size="md" onChange={(e) => setRelativeCancer({ ...relativeCancer, [e.target.name]: e.target.value })} />
                              </Col>
                              <Col md="6">
                                <Radios options={presenting_symptoms} text={relative + " Presenting Symptoms"} type="radio" name={"presenting_symptom_" + relative} id={"presenting_symptom_" + relative} htmlFor={"presenting_symptom_" + relative} size="md" onChange={(e) => setRelativeCancer({ ...relativeCancer, [e.target.name]: e.target.value })} />
                              </Col>
                            </Row>                          
                          </>
                        ))
                      )
                    }
                  </Col>
                  )}

                <div className="clearfix d-block w-100"><hr className="hr" /></div>
                
                <Col md="4" style={{zIndex:"0"}}>
                  <Radios options={co_morbodities} label={errors.co_morboditie && <>{errors.co_morboditie}</>} text="Co-morbidities?" type="radio" name="co_morboditie" id="co_morboditie" size="md" onChange={(e) => setDemography({ ...demography, co_morboditie: e.target.value })} />
                </Col>
                {demography.co_morboditie === "Other" && (
                  <Col md="2">
                  <Inputs label={errors.other_co_morboditie && <>{errors.other_co_morboditie}</>} text="Other Co-morbidities" type="text" name="other_co_morboditie" placeholder="Other Co-morbidities" id="other_co_morboditie" htmlfor="other_co_morboditie" size="md" onChange={(e) => setDemography({ ...demography, other_co_morboditie: e.target.value })} />                    
                </Col>
                )}
                
                <div className="clearfix d-block w-100"><hr className="hr" /></div>

                <h4 className="p-2">Addiction</h4>  
                <div className="clearfix d-block w-100"></div>       

                <Col md="2">
                  <RSelect options={yes_no} text="Tobacco" placeholder="Select Tobacco" name="tobacco" label={errors.tobacco && <>{errors.tobacco}</>} onChange={(e) => setDemography({ ...demography, tobacco: e.target.value })} id="tobacco" htmlfor="tobacco" size="md" />
                </Col>
                {demography.tobacco === "Yes" && (
                  <Col md="2">
                    <MultiSelect options={intake_types} text="Intake Type" placeholder="Select Intake Type" name="intake_type" label={errors.intake_type && <>{errors.intake_type}</>} onChange={(e) => setDemography({ ...demography, intake_type: Array.isArray(e) ? e.map(x => x.value) : [] })} id="intake_type" htmlfor="intake_type" size="md" />
                  </Col>
                  
                )}
                {
                  demography.intake_type.length > 0 && (
                    demography.intake_type.map((intake) => (
                      <Col md="2">
                        <Inputs label={errors.number_of_years && <>{errors.number_of_years}</>} text={intake + " Number of Years"} type="number" name={"number_of_years_"+intake} placeholder={intake + " Number of Years"} id={"number_of_years_"+intake} htmlfor={"number_of_years_"+intake} size="md" onChange={(e) => setIntakeType({ ...intakeType, [e.target.name]: e.target.value })} />
                      </Col>
                    ))
                  )
                }
                
                <div className="clearfix d-block w-100"></div>       

                <Col md="2">
                  <RSelect options={yes_no} text="Alcohol" placeholder="Select Alcohol" name="alcohol" label={errors.alcohol && <>{errors.alcohol}</>} onChange={(e) => setDemography({ ...demography, alcohol: e.target.value })} id="alcohol" htmlfor="alcohol" size="md" />
                </Col>
                {demography.alcohol === "Yes" && (
                  <>
                    <Col md="2">
                      <Inputs label={errors.number_of_pegs && <>{errors.number_of_pegs}</>} text="Number of Peg per Day (ml)" type="number" name="number_of_pegs" placeholder="Number of Peg per Day (ml)" id="number_of_pegs" htmlfor="number_of_pegs" size="md" onChange={(e) => setDemography({ ...demography, number_of_pegs: e.target.value })} />
                    </Col>
                    <Col md="3">
                      <Inputs label={errors.number_of_years_alcohol_addiction && <>{errors.number_of_years_alcohol_addiction}</>} text="Number of Years of Alcohol Addiction" type="number" name="number_of_years_alcohol_addiction" placeholder="Number of Years of Alcohol Addiction" id="number_of_years_alcohol_addiction" htmlfor="number_of_years_alcohol_addiction" size="md" onChange={(e) => setDemography({ ...demography, number_of_years_alcohol_addiction: e.target.value })} />
                    </Col>
                  </>
                )}
                
                <div className="clearfix d-block w-100"></div> 

                <Col md="2">
                  <RSelect options={diets} text="Diet" placeholder="Select Diet" name="diet" label={errors.diet && <>{errors.diet}</>} onChange={(e) => setDemography({ ...demography, diet: e.target.value })} id="diet" htmlfor="diet" size="md" />
                </Col>

              </Row>
              
              <Col md="3" className="p-0 pull-right">
                <Button type="submit" color="primary" onClick={onFormSubmit}>
                  Next {loading ? <Spinner size="sm" color="light" /> : ""}
                </Button>                          
              </Col>
            </Form>  
            {successMessage && (
              <div className="mb-3 mt-3">
                <Alert color="success" className="alert-icon">
                  {" "}
                  <Icon name="alert-circle" /> {successMessage}{" "}
                </Alert>
              </div>
            )}          
          </PreviewCard>
        </Block>        
      </PageContainer>
    </>
  );
};
export default DemographyInputs;

import React from "react";
// import Pages from "../route/Index";
import Content from "../adminLayout/Layouts/Content";
import { Block } from "../adminComponents/Component";
import { Col, Row } from "reactstrap";
import DemographyInputs from "./DemographyInputs";

const Demography = () => {

  //Sidebar  

  return (
    <React.Fragment>
      <Content>
        <Block>
          <Row className="g-gs">
            <Col xxl="12" className="container-fluid">
              <DemographyInputs />
            </Col>           
          </Row>
        </Block>
      </Content>
    </React.Fragment>
  );
};
export default Demography;

import React, { useState } from "react";
// import Pages from "../route/Index";
import Content from "../adminLayout/Layouts/Content";
import { Block, BlockBetween, BlockHead, BlockHeadContent, BlockTitle, Icon } from "../adminComponents/Component";
import { Button, Col, Row } from "reactstrap";
import DashboardBox from "../adminComponents/DashboardBox";
import { LoginToast, AllUsers } from "../adminComponents/DashboardCount";
// import AllMembersData from "./AllMembersData";
import { Link } from "react-router-dom";
import AllMembersData from "./AllPatientsData";

const Dashboard = () => {
  //Sidebar
  const [sm, updateSm] = useState(false);

  return (
    <React.Fragment>
      <Content>
      <BlockHead size="sm">
          <BlockBetween>
            <BlockHeadContent>
              <BlockTitle page tag="h3">
                Dashboard
              </BlockTitle>
            </BlockHeadContent>
            <BlockHeadContent>
              <div className="toggle-wrap nk-block-tools-toggle">
                <Button
                  className={`btn-icon btn-trigger toggle-expand mr-n1 ${sm ? "active" : ""}`}
                  onClick={() => updateSm(!sm)}
                >
                  <Icon name="more-v" />
                </Button>                
              </div>
            </BlockHeadContent>
          </BlockBetween>
        </BlockHead>
        <Block>
          <Row className="g-gs">
            <LoginToast />  
            <Col xxl="3" sm="6">
              <Link className="thumbs" to="/"><DashboardBox
                title="Total Users"
                count={<AllUsers />}
              /></Link>
            </Col>
            <Col xxl="3" sm="6">
              <Link className="thumbs" to="/"><DashboardBox
                title="Total Patients"
                count={<AllUsers />}
              /></Link>
            </Col>
            <Col xxl="3" sm="6">
              <Link className="thumbs" to="/"><DashboardBox
                title="Total Pending Patients"
                count={<AllUsers />}
              /></Link>
            </Col>
            <Col xxl="3" sm="6">
              <Link className="thumbs" to="/"><DashboardBox
                title="Messages"
                count={<AllUsers />}
              /></Link>
            </Col>            
            <Col xxl="12" md="12" lg="12" className="p-0 w-100">
            <BlockHead size="sm">
              <BlockBetween>
                <BlockHeadContent>
                  <BlockTitle page tag="h3">
                    All Patients
                  </BlockTitle>
                </BlockHeadContent>
              </BlockBetween>
            </BlockHead>
            <AllMembersData />              
            </Col>            
          </Row>
        </Block>
      </Content>
    </React.Fragment>
  );
};
export default Dashboard;

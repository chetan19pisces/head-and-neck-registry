import React from "react";
// import Pages from "../route/Index";
import Content from "../adminLayout/Layouts/Content";
import { Block } from "../adminComponents/Component";
import { Col, Row } from "reactstrap";
import InitialPresentationInputs from "./InitialPresentationInputs";

const InitialPresentation = () => {

  //Sidebar  

  return (
    <React.Fragment>
      <Content>
        <Block>
          <Row className="g-gs">
            <Col xxl="12" className="container-fluid">
              <InitialPresentationInputs />
            </Col>           
          </Row>
        </Block>
      </Content>
    </React.Fragment>
  );
};
export default InitialPresentation;
